package kz.openspace.example.hcity;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.Where;
import kz.openspace.sdk.modules.CategoryChildPresenter;
import okhttp3.HttpUrl;

public class NewsChildPresenter extends CategoryChildPresenter {

    public NewsChildPresenter(Context context, int category) {
        super(context, category);
        this.CATEGORY = "view";
        useLanguageParam(false);
    }

    @Override
    public HttpUrl.Builder getHostUrl() {
        return null;
    }

   /* @Override
    public String[] getHostUrl() {
        return new String[] {"adcloud.openspace.kz", "api", "v1", "news", "get"};
    }*/

    @Override
    public List jsonToCategoryItems(String s) {
        JsonParser parser = new JsonParser();
        JsonElement parse = parser.parse(s).getAsJsonObject();
        JsonElement array = parse.getAsJsonObject().getAsJsonArray("content");
        Gson gson = OpenSpaceUtils.createSugarAdaptedGson();
        List<Article> news = gson.fromJson(array, new TypeToken<List<Article>>(){}.getType());
        return news;
    }

    @Override
    public DiskRepository getDiskRepository() {
        DiskRepository<Article> diskRepository = new DiskRepository<>(Article.class, 100);
        diskRepository.addWhereClause(new Where("CATEGORY", getCategory()));
        return diskRepository;
    }
}
