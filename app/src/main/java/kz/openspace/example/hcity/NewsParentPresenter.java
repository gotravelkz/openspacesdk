package kz.openspace.example.hcity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.example.R;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.model.Category;
import kz.openspace.sdk.modules.CategoriesPresenter;


public class NewsParentPresenter extends CategoriesPresenter {

    public NewsParentPresenter(Context context) {
        super(context);
    }

    @Override
    public List jsonToCategories(String s) {
        return null;
    }

    @Override
    public DiskRepository getDiskRepository() {
        return null;
    }

    @Override
    public void loadData(Intent source) {
        Resources resources = ((Context) getView()).getResources();
        int[] categoryIds = resources.getIntArray(R.array.newsCategoryIds);
        String[] categoryNames = resources.getStringArray(R.array.newsCategoryNames);
        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < categoryIds.length; i++) {
            NewsCategory category = new NewsCategory(categoryIds[i], categoryNames[i]);
            categories.add(category);
        }
        getView().showData(categories);
    }

    @Override
    public String getUrl() {
        return "";
    }
}
