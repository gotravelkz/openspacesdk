package kz.openspace.example.hcity;

import android.os.Bundle;

import kz.openspace.sdk.adapter.CategoryPagerAdapter;
import kz.openspace.sdk.modules.CategoriesActivity;
import kz.openspace.sdk.modules.CategoriesPresenter;

public class NewsParentView extends CategoriesActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public CategoryPagerAdapter getCategoryAdapter() {
        return new CategoryPagerAdapter(getSupportFragmentManager(), NewsChildView.class);
    }

    @Override
    public CategoriesPresenter getPresenter() {
        return new NewsParentPresenter(this);
    }
}
