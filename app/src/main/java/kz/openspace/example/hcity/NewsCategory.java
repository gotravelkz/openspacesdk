package kz.openspace.example.hcity;

import kz.openspace.sdk.model.Category;

public class NewsCategory implements Category {
    private int categoryId;
    private String name;

    public NewsCategory() {
    }

    public NewsCategory(int categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getCategoryId() {
        return categoryId;
    }

    @Override
    public String getName() {
        return name;
    }
}
