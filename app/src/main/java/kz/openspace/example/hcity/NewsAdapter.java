package kz.openspace.example.hcity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kz.openspace.example.R;
import kz.openspace.sdk.adapter.LazyItemsAdapter;


public class NewsAdapter extends LazyItemsAdapter<NewsAdapter.ArticleHolder> {

    public NewsAdapter(Context context) {
        super(context);
    }

    @Override
    public ArticleHolder onCreateItemHolder(ViewGroup viewGroup, int i) {
        View view = getLayoutInflater().inflate(R.layout.item_news_article, viewGroup, false);
        return new ArticleHolder(view);
    }

    @Override
    public void onBindItemHolder(ArticleHolder articleHolder, int i) {
        Article article = (Article) getDataItems().get(i);
        articleHolder.name.setText(article.getName());
    }

    class ArticleHolder extends RecyclerView.ViewHolder{
        private TextView name;

        public ArticleHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }

}
