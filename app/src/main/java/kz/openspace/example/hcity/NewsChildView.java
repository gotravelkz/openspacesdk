package kz.openspace.example.hcity;

import kz.openspace.sdk.adapter.LazyItemsAdapter;
import kz.openspace.sdk.modules.CategoryChildFragment;
import kz.openspace.sdk.modules.CategoryChildPresenter;

public class NewsChildView extends CategoryChildFragment {

    @Override
    public CategoryChildPresenter getPresenter() {
        return new NewsChildPresenter(getContext(), getCategory());
    }

    @Override
    public LazyItemsAdapter getAdapter() {
        return new NewsAdapter(getContext());
    }
}
