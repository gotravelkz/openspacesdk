package kz.openspace.example.openline;

import android.content.Context;

import kz.openspace.sdk.modules.OpenlinePresenter;
import okhttp3.HttpUrl;

public class OpenlinePresenterImpl extends OpenlinePresenter {
    public static final String OPENLINE_HOST = "api.openspace.kz";
    public static final String OPENLINE_PATH = "openline";
    public static final String OPENLINE_STATUS_PATH = "openline_status";
    public static final String OPENLINE_ANSWERS = "http://api.openspace.kz/openline";
    private static final String USERNAME = "username";

    public OpenlinePresenterImpl(Context context) {
        super(context);
    }

    @Override
    public HttpUrl.Builder getOpenlineUrlBuilder() {
        return new HttpUrl.Builder()
                .scheme("http")
                .host(OPENLINE_HOST)
                .addPathSegment(OPENLINE_PATH);
    }

    @Override
    public String getAnswerUrl() {
        return OPENLINE_ANSWERS;
    }

    @Override
    public String getStatusUrl() {
        return new HttpUrl.Builder()
                .scheme("http")
                .host(OPENLINE_HOST)
                .addPathSegment(OPENLINE_STATUS_PATH)
                .addQueryParameter(USERNAME, getUsername())
                .build().toString();
    }

    @Override
    public String getApplicationName() {
        return "mtravel";
    }

    @Override
    public String getUsername() {
        return "900309300127";
    }
}
