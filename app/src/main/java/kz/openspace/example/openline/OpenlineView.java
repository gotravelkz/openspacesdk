package kz.openspace.example.openline;

import kz.openspace.sdk.modules.OpenlineActivity;
import kz.openspace.sdk.modules.OpenlinePresenter;

public class OpenlineView extends OpenlineActivity {

    @Override
    public OpenlinePresenter getPresenter() {
        return new OpenlinePresenterImpl(this);
    }

}
