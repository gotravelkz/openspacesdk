package kz.openspace.example.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;

import java.util.List;

public class Route extends SugarRecord{
    @SerializedName("id")
    @Unique
    public int routeId;
    public String name;
    private String text;
    private String contacts;
    @Ignore
    public List<TravelObject> objects;
    @Ignore
    private String[] images;
    @Expose
    private String imageUris;

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String[] getImages() {
        return images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public List<TravelObject> getObjects() {
        return TravelObject.find(TravelObject.class, "route = ?", getRouteId().toString());
    }

    public String getImageURIs() {
        return imageUris;
    }

    public void setImageURIs(String imageURIs) {
        this.imageUris = imageURIs;
    }
}
