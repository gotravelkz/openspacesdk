package kz.openspace.example.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import kz.openspace.sdk.model.Category;

public class ObjectCategory extends SugarRecord implements Category{

    @SerializedName("id")
    private int categoryId;
    private String name;

    @Override
    public int getCategoryId() {
        return categoryId;
    }

    @Override
    public String getName() {
        return name;
    }
}
