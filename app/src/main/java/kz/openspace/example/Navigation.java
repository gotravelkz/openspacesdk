package kz.openspace.example;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import kz.openspace.sdk.modules.NavigationMenu;
import kz.openspace.sdk.ui.NavigationItem;

public class Navigation extends NavigationMenu {

    public Navigation(Activity activity, int navigationViewId, int drawerLayoutId, int bottomNavigationId, int toolbarId) {
        super(activity, navigationViewId, drawerLayoutId, bottomNavigationId, toolbarId);
    }

    @Override
    public int getMenuResource() {
        return R.menu.menu_main;
    }

    @Override
    public int getHeaderResource() {
        return R.layout.header;
    }

    @Override
    public int[] getBottomNavigationIcons() {
        return new int[]{R.drawable.news, R.drawable.objects, R.drawable.space_id};
    }

    @Override
    public String[] getBottomNavigationTitles() {
        return new String[]{"Dribbble", "Chat", "Routes"};
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onNavItemClick(NavigationItem navigationItem, int position) {

    }
}
