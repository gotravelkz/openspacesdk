package kz.openspace.example;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.modules.BarcodeScannerActivity;

public class BarCodeExample extends AppCompatActivity {
    private static final int BARCODE_INTENT_REQUEST_CODE = 2597;
    private Button scanQrCodeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code_example);

        scanQrCodeButton = (Button) findViewById(R.id.scan);
        scanQrCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permission = ActivityCompat.checkSelfPermission(BarCodeExample.this, Manifest.permission.CAMERA);

                if (OpenSpaceUtils.isHigherThanMarshmallow() && permission != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(BarCodeExample.this, new String[]{Manifest.permission.CAMERA}, 2241);
                    return;
                }
                startBarCodeActivity();

            }
        });

    }

    private void startBarCodeActivity(){
        Intent intent = new Intent(this, BarcodeScannerActivity.class);
        startActivityForResult(intent, BARCODE_INTENT_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BARCODE_INTENT_REQUEST_CODE && data.hasExtra(BarcodeScannerActivity.BARCODE_CONTENT)){
            Toast.makeText(this, data.getStringExtra(BarcodeScannerActivity.BARCODE_CONTENT), Toast.LENGTH_SHORT).show();
        }
    }
}
