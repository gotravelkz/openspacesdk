package kz.openspace.example.routes;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

import kz.openspace.example.model.Route;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.modules.RoutesPresenter;
import okhttp3.HttpUrl;

public class RoutesPresenterImpl extends RoutesPresenter{
    private DiskRepository<Route> diskRepository;

    @Override
    protected HttpUrl.Builder getRoutesUrlBuilder() {
        return new HttpUrl.Builder().scheme("http").host("api.mangystau.info").addPathSegment("routes");
    }

    @Override
    public DiskRepository getDiskRepository() {
        diskRepository = new DiskRepository<>(Route.class, 40);
        return diskRepository;
    }

    @Override
    public List jsonToRoutes(String json) {
        Gson gson = OpenSpaceUtils.createSugarAdaptedGson();
        List<Route> list = gson.fromJson(json, new TypeToken<List<Route>>() {}.getType());
        for (Route route : list) {
            String imgs = "";
            for (String s : route.getImages()) {
                imgs += "[" + s + "]";
            }
            route.setImageURIs(imgs);
        }
        return list;
    }

}
