package kz.openspace.example.routes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kz.openspace.example.R;
import kz.openspace.example.model.Route;
import kz.openspace.example.model.TravelObject;
import kz.openspace.sdk.adapter.ItemSelectedListener;
import kz.openspace.sdk.adapter.LazyItemsAdapter;

public class RoutesAdapter extends LazyItemsAdapter<RoutesAdapter.RouteHolder> {
    private final String REGEXP = "(?<=\\[)\\w.*?(?=])";
    Picasso picasso;
    private ItemSelectedListener itemSelectedListener;

    public RoutesAdapter(Context context) {
        super(context);
        picasso = Picasso.with(context);
    }

    public void setItemSelectedListener(ItemSelectedListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @Override
    public RouteHolder onCreateItemHolder(ViewGroup parent, int viewType) {
        return new RouteHolder(getLayoutInflater().inflate(R.layout.item_route, parent, false));
    }

    @Override
    public void onBindItemHolder(RouteHolder holder, int position) {
        Route route = (Route) getDataItems().get(position);
        holder.name.setText(route.getName());
        holder.description.setText(route.getText());
        String imageUrl = getImageUrl(route.getImageURIs());
        if (imageUrl != null)
            picasso
                .load(imageUrl)
                .fit()
                .centerCrop()
                .into(holder.image);
    }

    public String getImageUrl(String images){
        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(images);
        matcher.find();
        return matcher.group();
    }

    class RouteHolder extends RecyclerView.ViewHolder{
        private TextView name;
        private ImageView image;
        public TextView description;

        public RouteHolder(final View itemView) {
            super(itemView);
            description = (TextView) itemView.findViewById(R.id.description);
            name = (TextView) itemView.findViewById(R.id.name);
            image = (ImageView) itemView.findViewById(R.id.image);
            itemView.findViewById(R.id.root).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.onItemSelected(getDataItems().get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }

}
