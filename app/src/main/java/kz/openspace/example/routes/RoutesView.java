package kz.openspace.example.routes;

import android.content.Intent;
import android.os.Bundle;

import kz.openspace.example.Navigation;
import kz.openspace.example.R;
import kz.openspace.example.login.LoginView;
import kz.openspace.sdk.adapter.ItemSelectedListener;
import kz.openspace.sdk.adapter.LazyItemsAdapter;
import kz.openspace.sdk.annotation.Navigated;
import kz.openspace.sdk.modules.RoutesActivity;
import kz.openspace.sdk.modules.RoutesPresenter;

@Navigated(value = Navigation.class, bottomSelectedItem = 0)
public class RoutesView extends RoutesActivity implements ItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        super.onCreate(savedInstanceState);
    }

    @Override
    public RoutesPresenter getRoutesPresenter() {
        return new RoutesPresenterImpl();
    }

    @Override
    public LazyItemsAdapter getAdapter() {
        RoutesAdapter adapter = new RoutesAdapter(this);
        adapter.setItemSelectedListener(this);
        return adapter;
    }

    @Override
    public void onItemSelected(Object item, int position) {
        Intent intent = new Intent(this, LoginView.class);
        startActivity(intent);
    }
}
