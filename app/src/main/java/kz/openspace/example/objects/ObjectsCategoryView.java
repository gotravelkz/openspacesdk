package kz.openspace.example.objects;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import kz.openspace.example.PlayerActivityExample;
import kz.openspace.sdk.adapter.CategoryPagerAdapter;
import kz.openspace.sdk.modules.CategoriesActivity;
import kz.openspace.sdk.modules.CategoriesPresenter;

public class ObjectsCategoryView extends CategoriesActivity {

    @Override
    public CategoryPagerAdapter getCategoryAdapter() {
        return new CategoryPagerAdapter(getSupportFragmentManager(), ObjectsView.class);
    }

    @Override
    public CategoriesPresenter getPresenter() {
        return new ObjectsCategoryPresenterImpl(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,1,0, "AudioPlayer");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 1:
                Intent intent = new Intent(this, PlayerActivityExample.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
