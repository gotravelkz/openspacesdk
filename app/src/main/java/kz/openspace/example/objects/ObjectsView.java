package kz.openspace.example.objects;

import kz.openspace.sdk.adapter.LazyItemsAdapter;
import kz.openspace.sdk.modules.CategoryChildFragment;
import kz.openspace.sdk.modules.CategoryChildPresenter;

public class ObjectsView extends CategoryChildFragment {
    @Override
    public CategoryChildPresenter getPresenter() {
        return new CategoryChildPresenterImpl(getContext(), getCategory());
    }

    @Override
    public LazyItemsAdapter getAdapter() {
        return new ObjectsAdapter(getContext());
    }
}
