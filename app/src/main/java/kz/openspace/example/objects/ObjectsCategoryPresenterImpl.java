package kz.openspace.example.objects;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import kz.openspace.example.model.ObjectCategory;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.modules.CategoriesPresenter;

public class ObjectsCategoryPresenterImpl extends CategoriesPresenter {

    public ObjectsCategoryPresenterImpl(Context context) {
        super(context);
    }

    @Override
    public List jsonToCategories(String json) {
        Gson gson = OpenSpaceUtils.createSugarAdaptedGson();
        List<ObjectCategory> list = gson.fromJson(json, new TypeToken<List<ObjectCategory>>(){}.getType());
        return list;
    }

    @Override
    public DiskRepository getDiskRepository() {
        return new DiskRepository(ObjectCategory.class, 0);
    }

    @Override
    public String getUrl() {
        return "http://api.mangystau.info/objects_categories";
    }
}
