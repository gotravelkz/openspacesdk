package kz.openspace.example.objects;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import kz.openspace.example.model.TravelObject;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.Where;
import kz.openspace.sdk.modules.CategoryChildPresenter;
import okhttp3.HttpUrl;

/**
 * Created by mancj on 20.09.2016.
 */

public class CategoryChildPresenterImpl extends CategoryChildPresenter {

    public CategoryChildPresenterImpl(Context context, int category) {
        super(context, category);
    }

    @Override
    public HttpUrl.Builder getHostUrl() {
        return null;
    }

   /* @Override
    public String[] getHostUrl() {
        return new String[]{"api.mangystau.info", "objects"};
    }*/

    @Override
    public List jsonToCategoryItems(String json) {
        Gson gson = OpenSpaceUtils.createSugarAdaptedGson();
        List<TravelObject> list = gson.fromJson(json, new TypeToken<List<TravelObject>>(){}.getType());
        return list;
    }

    @Override
    public DiskRepository getDiskRepository() {
        DiskRepository repo = new DiskRepository(TravelObject.class, 40);
        repo.addWhereClause(new Where("category", getCategory()));
        return repo;
    }
}
