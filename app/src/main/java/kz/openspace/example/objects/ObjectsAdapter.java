package kz.openspace.example.objects;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kz.openspace.example.R;
import kz.openspace.example.model.TravelObject;
import kz.openspace.sdk.adapter.LazyItemsAdapter;

/**
 * Created by mancj on 20.09.2016.
 */

public class ObjectsAdapter extends LazyItemsAdapter<ObjectsAdapter.ObjectHolder> {

    public ObjectsAdapter(Context context) {
        super(context);
    }

    @Override
    public ObjectHolder onCreateItemHolder(ViewGroup parent, int viewType) {
        return new ObjectHolder(getLayoutInflater().inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindItemHolder(ObjectHolder holder, int position) {
        TravelObject travelObject = (TravelObject) getDataItems().get(position);
        holder.mText.setText(travelObject.getName());
    }

    class ObjectHolder extends RecyclerView.ViewHolder{
        TextView mText;

        public ObjectHolder(View itemView) {
            super(itemView);
            mText = (TextView) itemView.findViewById(R.id.mText);
        }
    }

}
