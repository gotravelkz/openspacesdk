package kz.openspace.example;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.model.Track;
import kz.openspace.sdk.modules.AudioPlayerFragment;

public class PlayerActivityExample extends AppCompatActivity {
    public static final String URL = "http://www.noiseaddicts.com/samples_1w72b820/142.mp3";
    public static final String FILE_PATH = Environment.getExternalStorageDirectory() + "/OpenSpace";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_example);
        List<Track> tracks = new ArrayList<>();
        tracks.add(new Track("1.mp3", "https://acm.math.spbu.ru/speeches/great%20speeches%20-%201987.mp3"));
        tracks.add(new Track("2.mp3", "https://acm.math.spbu.ru/speeches/Brezhnev_30_Pobeda_v_eti_torjestvennye_dni.mp3"));
        tracks.add(new Track("3.mp3", "https://acm.math.spbu.ru/speeches/Kalinin1.mp3"));
        AudioPlayerFragment fragment = (AudioPlayerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        fragment.setFilesDir(FILE_PATH);
        fragment.setTracks(tracks);

//        SimpleAudioPlayerFragment fragment = (SimpleAudioPlayerFragment) getSupportFragmentManager().findFragmentById(R.id.audioFragment);
//        createFolderForAudio();
//        fragment.setDownloadUrl(URL);
//        fragment.setFilePath(FILE_PATH);
    }

    private void createFolderForAudio(){
        File file = new File(Environment.getExternalStorageDirectory() + "/OpenSpace");
        if (!file.exists())
            file.mkdir();
    }

}
