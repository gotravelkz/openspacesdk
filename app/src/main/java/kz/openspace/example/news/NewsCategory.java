package kz.openspace.example.news;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import kz.openspace.sdk.model.Category;

public class NewsCategory extends SugarRecord implements Category{

    @Unique
    @Expose
    @SerializedName("id")
    private int categoryId;
    @Expose
    private String name;

    @Override
    public int getCategoryId() {
        return categoryId;
    }

    @Override
    public String getName() {
        return name;
    }
}
