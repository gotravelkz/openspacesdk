package kz.openspace.example.news;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.List;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.Where;
import kz.openspace.sdk.modules.CategoryChildPresenter;
import okhttp3.HttpUrl;

public class CategoryChildPresenterImpl extends CategoryChildPresenter {

    public CategoryChildPresenterImpl(Context context, int category) {
        super(context, category);
    }

    @Override
    public HttpUrl.Builder getHostUrl() {
        return null;
    }

    @Override
    public List jsonToCategoryItems(String json) {
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(json).getAsJsonArray();
        List<Article> list = new ArrayList<>();
        for (JsonElement element : array) {
            int cat = element.getAsJsonObject().get("category_id").getAsInt();
            String name = element.getAsJsonObject().get("title").getAsString();
            String description = element.getAsJsonObject().get("text").getAsString();
            Article article = new Article();
            article.setCategory(cat);
            article.setName(name);
            article.setDescription(description);
            list.add(article);
        }
        return list;
    }

   /* @Override
    public String[] getHostUrl() {
        return new String[]{"api.mangystau.info", "news"};
    }*/

    @Override
    public DiskRepository getDiskRepository() {
        DiskRepository<Article> diskRepository = new DiskRepository<>(Article.class, 40);
        diskRepository.addWhereClause(new Where("CATEGORY", getCategory()));
        return diskRepository;
    }

}
