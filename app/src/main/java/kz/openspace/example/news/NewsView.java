package kz.openspace.example.news;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import kz.openspace.example.Navigation;
import kz.openspace.example.R;
import kz.openspace.sdk.annotation.Navigated;
import kz.openspace.sdk.modules.CategoriesActivity;
import kz.openspace.sdk.modules.CategoriesPresenter;
import kz.openspace.sdk.adapter.CategoryPagerAdapter;
import kz.openspace.sdk.ui.BottomNavigationView;

@Navigated(value = Navigation.class, bottomSelectedItem = 1)
public class NewsView extends CategoriesActivity {
    private BottomNavigationView bottomNavigationView;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
    }

    @Override
    public CategoryPagerAdapter getCategoryAdapter() {
        return new CategoryPagerAdapter(getSupportFragmentManager(), CategoryChildView.class);
    }

    @Override
    public CategoriesPresenter getPresenter() {
        return new CategoriesPresenterImpl(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
