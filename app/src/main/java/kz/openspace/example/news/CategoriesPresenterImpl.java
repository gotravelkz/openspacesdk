package kz.openspace.example.news;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Locale;

import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.model.Category;
import kz.openspace.sdk.modules.CategoriesPresenter;
import okhttp3.HttpUrl;

/**
 * Created by mancj on 02.09.2016.
 */

public class CategoriesPresenterImpl extends CategoriesPresenter {

    public CategoriesPresenterImpl(Context context) {
        super(context);
    }

    @Override
    public List<Category> jsonToCategories(String json) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        List categories = gson.fromJson(json, new TypeToken<List<NewsCategory>>(){}.getType());
        return categories;
    }

    @Override
    public DiskRepository getDiskRepository() {
        return new DiskRepository(NewsCategory.class, 0);
    }

    @Override
    public String getUrl() {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host("api.mangystau.info")
                .addPathSegment("news_categories")
                .addQueryParameter("lang", Locale.getDefault().getCountry().toLowerCase())
                .build();
        Log.d("LOG_TAG", getClass().getSimpleName() + " url " + url.toString());
        return url.toString();
    }

}
