package kz.openspace.example.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kz.openspace.example.R;
import kz.openspace.sdk.adapter.LazyItemsAdapter;

public class NewsAdapter extends LazyItemsAdapter<NewsAdapter.ItemHolder> {

    public NewsAdapter(Context context) {
        super(context);
    }

    @Override
    public ItemHolder onCreateItemHolder(ViewGroup parent, int viewType) {
        View view = getLayoutInflater().inflate(R.layout.item, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindItemHolder(ItemHolder holder, int position) {
        Article article = (Article) getDataItems().get(position);
        holder.mText.setText(article.getName().toString());
    }


    static class ItemHolder extends RecyclerView.ViewHolder{
        private TextView mText;

        public ItemHolder(View itemView) {
            super(itemView);
            this.mText = (TextView) itemView.findViewById(R.id.mText);
        }
    }
}
