package kz.openspace.example.news;

import kz.openspace.sdk.adapter.LazyItemsAdapter;
import kz.openspace.sdk.modules.CategoryChildFragment;
import kz.openspace.sdk.modules.CategoryChildPresenter;

public class CategoryChildView extends CategoryChildFragment {

    @Override
    public CategoryChildPresenter getPresenter() {
        return new CategoryChildPresenterImpl(getContext(), getCategory());
    }

    @Override
    public LazyItemsAdapter getAdapter() {
        return new NewsAdapter(getContext());
    }

}
