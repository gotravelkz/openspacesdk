package kz.openspace.example.news;

import android.support.annotation.Nullable;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Article extends SugarRecord {
    @Unique
    private String name;
    private String description;
    private int category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
