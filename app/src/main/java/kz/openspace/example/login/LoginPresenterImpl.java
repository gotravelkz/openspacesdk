package kz.openspace.example.login;

import kz.openspace.sdk.modules.LoginPresenter;

/**
 * Created by mancj on 14.09.2016.
 */

public class LoginPresenterImpl extends LoginPresenter {

    @Override
    public String getAuthUrl() {
        return "http://api.openspace.kz/sms_auth";
    }

    @Override
    public String getApplicationName() {
        return "hcity";
    }

    @Override
    public String getToken() {
        return "sdfxcv234";
    }
}
