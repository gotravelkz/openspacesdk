package kz.openspace.example.login;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import kz.openspace.sdk.modules.AccountActivity;

public class AccountView extends AccountActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(android.R.string.cancel);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Auth auth = Auth.getInstance();
//        auth.logOut();
//        Intent intent = new Intent(this, LoginView.class);
//        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getToken() {
        return "null";
    }

    @Override
    public String getAppName() {
        return "hcity";
    }
}
