package kz.openspace.example.login;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import kz.openspace.example.R;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.data.Auth;
import kz.openspace.sdk.modules.LoginActivity;
import kz.openspace.sdk.modules.LoginPresenter;

public class LoginView extends LoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLogoResource() {
        return R.drawable.dribbble_xxl;
    }

    @Override
    public LoginPresenter getLoginPresenter() {
        return new LoginPresenterImpl();
    }

    @Override
    public void onSignIn() {
//        Auth auth = Auth.getInstance();
//        auth.logOut();
//        Intent intent = new Intent(this, AccountView.class);
//        startActivity(intent);
    }

    @Override
    public String getAuthInfoIntentAction() {
        return null;
    }

    @Override
    public String getAboutAppIntentAction() {
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
