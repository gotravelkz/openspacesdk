package kz.openspace.sdk.background;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import kz.openspace.sdk.model.Track;


public class AudioPlayerService extends Service implements MediaPlayer.OnCompletionListener {
    private PlayerBinder playerBinder = new PlayerBinder();
    private List<Track> tracks;
    private Thread downloadAudioThread;
    private String filesDir;
    private PlayerStateChangeListener playerStateChangeListener;
    private int downloadingFileIndex = -1;
    private TimerTask timerTask;
    private Timer timer;

    private MediaPlayer player;
    private int currentTrackIndex = -1;

    public void setFilesDir(String filesDir) {
        this.filesDir = filesDir;
    }

    protected void prepare(){
        try {
            player.reset();
            player.setDataSource(filesDir + "/" + tracks.get(currentTrackIndex).getName());
            player.prepareAsync();
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    startTimer();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startTimer(){
        timer = null;
        timerTask = null;
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (player.getDuration() > 0)
                {
                    int percent = player.getCurrentPosition() * 100 /player.getDuration();
                    Track track = tracks.get(currentTrackIndex);
                    track.setPosition(percent);
                    track.setPositionInMillis(player.getCurrentPosition());
                }
                if (playerStateChangeListener != null){
                    playerStateChangeListener.onPositionUpdate(player.getDuration(), player.getCurrentPosition());
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    public boolean isPlaying(){
        return player.isPlaying();
    }

    public void seekTo(int percent){
        int position = (player.getDuration() * percent / 100);
        player.seekTo(position);
    }

    public int getCurrentTrackIndex(){
        return currentTrackIndex;
    }

    public void play(int index){
        if (currentTrackIndex >= 0){
            Track track = tracks.get(currentTrackIndex);
            track.setPlaying(false);
            track.setPosition(0);
            track.setPositionInMillis(0);
        }
        if (currentTrackIndex == index){
            player.start();
            startTimer();
        }else {
            currentTrackIndex = index;
            prepare();
        }
        tracks.get(index).setPlaying(true);
    }

    public void pause(){
        timer.cancel();
        player.pause();
        tracks.get(currentTrackIndex).setPlaying(false);
    }

    public void setTracks(List<Track> tracks){
        this.tracks = tracks;
    }

    public void checkDownloadedTracks(){
        for (Track track : tracks) {
            File file = new File(filesDir + "/" + track.getName());
            if (file.exists())
                track.setDownloaded(true);
            else
                track.setDownloaded(false);
        }
    }

    public void downloadFile(final int index){
        downloadAudioThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    downloadingFileIndex = index;
                    Track track = tracks.get(index);
                    FileUtils.copyURLToFile(new URL(track.getUrl()), new File(filesDir + "/" + track.getName()));
                    if (playerStateChangeListener != null)
                        playerStateChangeListener.onReady();
                    track.setDownloaded(true);
                    downloadingFileIndex = -1;
                } catch (IOException e) {
                    Log.e("OpenSpaceSDK", AudioPlayerService.class.getSimpleName() + ": Cannot access specified path. " +
                            "1. Make sure you have specified a path; " +
                            "2. Make sure that the \"WRITE_EXTERNAL_STORAGE\" permission is granted;" );
                    e.printStackTrace();
                }
            }
        });
        downloadAudioThread.start();
    }

    public void cancelDownloading(){
        downloadAudioThread.interrupt();
        downloadAudioThread = null;
        Track track = tracks.get(downloadingFileIndex);
        track.setDownloaded(false);
        String fileName = filesDir + "/" + track.getName();
        File file = new File(fileName);
        file.delete();
        downloadingFileIndex = -1;
    }

    public void setPlayerStateChangeListener(PlayerStateChangeListener playerStateChangeListener) {
        this.playerStateChangeListener = playerStateChangeListener;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (tracks.size() > currentTrackIndex + 1){
            timer.cancel();
            Track currentTrack = tracks.get(currentTrackIndex);
            currentTrack.setPosition(0);
            currentTrack.setPlaying(false);
            currentTrack.setPositionInMillis(0);
            if (playerStateChangeListener != null){
                playerStateChangeListener.onReady();
            }
        }
    }

    public interface PlayerStateChangeListener {
        void onReady();
        void onPositionUpdate(int duration, int position);
    }

    @Override
    public IBinder onBind(Intent intent) {
        this.player = new MediaPlayer();
        player.setOnCompletionListener(this);
        return playerBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (timer != null)
        {
            timer.cancel();
        }
        if (downloadingFileIndex >= 0)
        {
            cancelDownloading();
        }
        if (currentTrackIndex >= 0 && currentTrackIndex < tracks.size() - 1)
        {
            tracks.get(currentTrackIndex).setPlaying(false);
        }
        playerStateChangeListener = null;
        tracks = null;
        player.release();
        player = null;
        return super.onUnbind(intent);
    }

    public class PlayerBinder extends Binder{
        public AudioPlayerService getService(){
            return AudioPlayerService.this;
        }
    }

}
