package kz.openspace.sdk.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;

public class User extends SugarRecord {

    @Unique
    private String phoneNumber;
    @SerializedName("id")
    private Long userId;
    @SerializedName("username")
    private Long iin;
    private String email;
    @SerializedName("first_name")
    private String firstname;
    @SerializedName("last_name")
    private String lastname;
    @SerializedName("middle_name")
    private String midname;
    @SerializedName("gender_id")
    private int gender;
    @SerializedName("age_id")
    private int age;
    @SerializedName("social_status_id")
    private int socialStatusId;
    @SerializedName("is_resident")
    private int isRKResident;
    private String img;
    @Ignore
    private String password;
    @SerializedName("phone_3_confirmed")
    private int authorized;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getIin() {
        return iin;
    }

    public void setIin(Long iin) {
        this.iin = iin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMidname() {
        return midname;
    }

    public void setMidname(String midname) {
        this.midname = midname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSocialStatusId() {
        return socialStatusId;
    }

    public void setSocialStatusId(int socialStatusId) {
        this.socialStatusId = socialStatusId;
    }

    public int isResident() {
        return isRKResident;
    }

    public void setIsRKResident(int isRKResident) {
        this.isRKResident = isRKResident;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getIsRKResident() {
        return isRKResident;
    }

    public int isAuthorized() {
        return authorized;
    }

    public void setAuthorized(int authorized) {
        this.authorized = authorized;
    }
}
