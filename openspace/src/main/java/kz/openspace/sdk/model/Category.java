package kz.openspace.sdk.model;

public interface Category{
    int getCategoryId();
    String getName();
}
