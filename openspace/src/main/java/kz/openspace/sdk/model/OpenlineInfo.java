package kz.openspace.sdk.model;

public class OpenlineInfo {
    private Integer answered;
    private Integer unanswered;
    private Integer userRate;

    public Integer getAnswered() {
        return answered;
    }

    public void setAnsweredCount(Integer answered) {
        this.answered = answered;
    }

    public Integer getUnanswered() {
        return unanswered;
    }

    public void setUnansweredCount(Integer unanswered) {
        this.unanswered = unanswered;
    }

    public Integer getUserRate() {
        return userRate;
    }

    public void setUserRate(Integer userRate) {
        this.userRate = userRate;
    }
}
