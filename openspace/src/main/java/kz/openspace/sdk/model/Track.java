package kz.openspace.sdk.model;

public class Track {
    private String name;
    private String url;
    private boolean playing;
    private boolean downloaded;
    private int position = 0;
    private long positionInMillis = 0;

    public Track() {
    }

    public Track(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public long getPositionInMillis() {
        return positionInMillis;
    }

    public void setPositionInMillis(long positionInMillis) {
        this.positionInMillis = positionInMillis;
    }
}
