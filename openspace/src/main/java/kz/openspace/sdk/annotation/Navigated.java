package kz.openspace.sdk.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import kz.openspace.sdk.modules.NavigationMenu;

/**
 * <p>Включает отображение NavigationView и BottomNavigationView в активити.<p/>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Navigated {
    /**
     * Нужен для создания класса наследника NavigationMenu с пользовательской реализацией
     * @return
     */
    Class<? extends NavigationMenu> value();

    /**
     * Номер выбранного элемента нижнего меню
     * @return
     */
    int bottomSelectedItem() default -1;
}
