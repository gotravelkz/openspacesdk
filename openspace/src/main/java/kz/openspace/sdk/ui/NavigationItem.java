package kz.openspace.sdk.ui;

public class NavigationItem {
    private int icon;
    private CharSequence title;

    public NavigationItem(int icon, CharSequence title) {
        this.icon = icon;
        this.title = title;
    }

    public NavigationItem() {
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

}
