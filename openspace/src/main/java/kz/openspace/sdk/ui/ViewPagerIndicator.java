package kz.openspace.sdk.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import kz.openspace.sdk.R;

public class ViewPagerIndicator extends View {
    private int itemsCount;
    private float destiny;
    private Rect[] indicators;
    private int size;
    private int margin;
    private Rect src;
    private Bitmap bitmap;
    private Bitmap bitmapPlus;
    private int active;

    public ViewPagerIndicator(Context context) {
        super(context);
        init(context);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        destiny = context.getResources().getDisplayMetrics().density;
        size = (int) (10*destiny);
        src = new Rect(0, 0, size, size);
        margin = (int) (size*1.5f);
        createBitmap();
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
        if (itemsCount > 10)
            this.itemsCount = 11;
        invalidate();
    }

    public void setActiveItem(int index){
        this.active = index;
        invalidate();
    }

    private void createBitmap(){
        Bitmap tmp  = BitmapFactory.decodeResource(getResources(), R.drawable.slider_indicator);
        Bitmap tmpPlus = BitmapFactory.decodeResource(getResources(), R.drawable.indicator_plus);
        bitmap = Bitmap.createScaledBitmap(tmp, size, size, true);
        bitmapPlus = Bitmap.createScaledBitmap(tmpPlus, size, size, true);
        tmp.recycle();
        tmpPlus.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), size*2);
    }

    private void drawIndicators(Canvas canvas){
        indicators = new Rect[itemsCount];
        int center = (size + margin * itemsCount)/2;
        int startPoint = getWidth()/2 - center;
        int drawStart = startPoint;
        Paint inactivePaint = new Paint();
        inactivePaint.setAntiAlias(true);
        inactivePaint.setAlpha(100);
        Paint activePaint = new Paint();
        activePaint.setAntiAlias(true);
        activePaint.setAlpha(255);
        for (int i = 0; i < itemsCount; i++) {
            Paint paint = i == active ? activePaint : inactivePaint;
            indicators[i] = new Rect(drawStart, 0, drawStart + size, size);
            drawStart = drawStart + margin;
            if (i < 10)
            {
                canvas.drawBitmap(bitmap, src, indicators[i], paint);
            }
            else
            {
                if (active > 9)
                    paint = activePaint;
                canvas.drawBitmap(bitmapPlus, src, indicators[i], paint);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawIndicators(canvas);
    }
}
