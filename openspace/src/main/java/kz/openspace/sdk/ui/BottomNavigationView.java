package kz.openspace.sdk.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.R;

public class BottomNavigationView extends LinearLayout implements View.OnClickListener, Animation.AnimationListener {
    private List<NavigationItem> items;
    private int selectedItem;
    private boolean showUnselectedText;
    private Animation animation;
    private Animation translateIn;
    private Animation translateOut;
    private boolean playAnim = true;
    private OnNavClickListener onNavClickListener;

    public BottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.BottomNavigationView);
        selectedItem = array.getInt(R.styleable.BottomNavigationView_defaultSelecedItem, 0);
        showUnselectedText = array.getBoolean(R.styleable.BottomNavigationView_showUnselectedText, true);

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER);
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        items = new ArrayList<>();
        animation =  AnimationUtils.loadAnimation(getContext(), R.anim.scale_in);
        translateIn =  AnimationUtils.loadAnimation(getContext(), R.anim.translate_in);
        translateOut =  AnimationUtils.loadAnimation(getContext(), R.anim.translate_out);
        translateOut.setAnimationListener(this);
        array.recycle();
        update();
    }

    private boolean hiding;

    public void hide(){
        if (getVisibility() == VISIBLE && !hiding)
        {
            hiding = true;
            startAnimation(translateOut);
        }
    }

    public void show(){
        if (getVisibility() != VISIBLE && !hiding)
        {
            setVisibility(VISIBLE);
            startAnimation(translateIn);
        }
    }

    public void setOnNavClickListener(OnNavClickListener onNavClickListener) {
        this.onNavClickListener = onNavClickListener;
    }

    public void addItem(int icon, CharSequence title){
        NavigationItem item = new NavigationItem(icon, title);
        items.add(item);
        addView(item);
        update();
    }

    public void addItems(CharSequence[] titles, int[] icons){
        if (titles.length != icons.length)
            throw new IllegalArgumentException("the size of the icons and titles arrays should be the same");
        for (int i = 0; i < icons.length; i++) {
            NavigationItem item = new NavigationItem(icons[i], titles[i]);
            items.add(item);
            addView(item);
        }
        update();
    }

    private void addView(NavigationItem item){
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.nav_item, this, false);
        TextView title = (TextView) linearLayout.findViewById(R.id.title);
        ImageView icon = (ImageView) linearLayout.findViewById(R.id.icon);
        title.setText(item.getTitle());
        icon.setImageResource(item.getIcon());
        LinearLayout.LayoutParams lp = (LayoutParams) linearLayout.getLayoutParams();
        int position = items.indexOf(item);
        linearLayout.setId(position);
        linearLayout.setOnClickListener(this);
        addView(linearLayout);
    }

    public void update(){
        for (int i = 0; i < items.size(); i++) {
            LinearLayout layout = (LinearLayout) findViewById(i);
            if (items.indexOf(items.get(i)) != selectedItem){
                layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                if (!showUnselectedText){
                    layout.findViewById(R.id.title).setVisibility(GONE);
                }
            }else {
                if (!showUnselectedText){
                    layout.findViewById(R.id.title).setVisibility(VISIBLE);
                    if (playAnim)
                        layout.findViewById(R.id.title).startAnimation(animation);
                }
                layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            }
        }
    }

    public void removeItem(int position){
        items.remove(position);
        removeView(findViewById(position));
    }

    public void setItemSelected(int position){
        playAnim = position == selectedItem ? false : true;
        this.selectedItem = position;
        update();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        setVisibility(GONE);
        hiding = false;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public interface OnNavClickListener {
        void onNavItemClick(NavigationItem navigationItem, int position);
    }

    @Override
    public void onClick(View view) {
        int position = view.getId();
        playAnim = position == selectedItem ? false : true;
        if (playAnim && onNavClickListener != null)
            onNavClickListener.onNavItemClick(items.get(position), position);
        selectedItem = position;
        update();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.selectedItem = this.selectedItem;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
        SavedState savedState = (SavedState)state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.selectedItem = savedState.selectedItem;
        update();
    }

    static class SavedState extends BaseSavedState{
        int selectedItem;

        public SavedState(Parcelable source) {
            super(source);
        }

        public SavedState(Parcel source) {
            super(source);
            this.selectedItem = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(selectedItem);
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
    }

}
