package kz.openspace.sdk.modules;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.R;
import kz.openspace.sdk.adapter.OpenLineAnsweredAdapter;
import kz.openspace.sdk.model.Answer;
import kz.openspace.sdk.model.OpenlineInfo;
import kz.openspace.sdk.model.Question;
import kz.openspace.sdk.ui.BottomNavigationView;
import kz.openspace.sdk.ui.NavigationItem;

public abstract class OpenlineActivity extends NavigatedActivity implements BaseView,
        NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavClickListener,
        RadioGroup.OnCheckedChangeListener, TabLayout.OnTabSelectedListener {

    private RadioGroup radioGroup;
    private TabLayout tabLayout;
    private OpenlinePresenter presenter;
    private TextView questionView;
    private float destiny;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private OpenLineAnsweredAdapter answersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.openline_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        radioGroup = (RadioGroup) findViewById(R.id.rGroup);
        questionView = (TextView) findViewById(R.id.question);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        answersAdapter = new OpenLineAnsweredAdapter(this);
        recyclerView.setAdapter(answersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setupTabs();

        presenter = getPresenter();
        presenter.setView(this);
        destiny = getResources().getDisplayMetrics().density;
        buildDialog();
        presenter.loadData(null);

        tabLayout.getTabAt(1).select();
        tabLayout.addOnTabSelectedListener(this);

        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void showData(Object data) {
        Question group = (Question) data;
        radioGroup.removeAllViews();
        questionView.setText(((Question) data).getName());
        for (Object answer : group.getAnswers()) {
            radioGroup.addView(createRadioButton(((Answer) answer).getName()));
        }
    }

    private RadioButton createRadioButton(String text){
        RadioButton radioButton = new RadioButton(this);
        RadioGroup.LayoutParams p = new RadioGroup.LayoutParams(-1, -2);
        int margin = (int) (8 * destiny);
        p.topMargin = margin;
        radioButton.setLayoutParams(p);
        radioButton.setText(text);
        radioButton.setPadding(margin, 0, 0, 0);
        return radioButton;
    }

    @Override
    public void showLoading(boolean loading) {
        if (loading)
            progressDialog.show();
        else
            progressDialog.dismiss();
    }

    public void showOpenlineInfo(final OpenlineInfo openlineInfo){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) tabLayout.getTabAt(0).getCustomView().findViewById(R.id.textView)).setText(openlineInfo.getAnswered().toString());
                ((TextView) tabLayout.getTabAt(1).getCustomView().findViewById(R.id.textView)).setText(openlineInfo.getUnanswered().toString());
                ((TextView) tabLayout.getTabAt(2).getCustomView().findViewById(R.id.textView)).setText(openlineInfo.getUserRate().toString());
                TextView rateCount = (TextView) findViewById(R.id.rateCount);
                TextView rateLeft = (TextView) findViewById(R.id.rateLeft);
                String count = getString(R.string.openline_rate_count);
                String left = getString(R.string.openline_rate_left);
                rateCount.setText(String.format(count, openlineInfo.getUserRate()));
                int rateLeftCount = openlineInfo.getUserRate() > 500 ? 0 : openlineInfo.getUserRate();
                rateLeft.setText(String.format(left, 500 - rateLeftCount));
            }
        });
    }

    protected void setAnswersQuestions(final List<Question> questions){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                answersAdapter.setQuestions(questions);
            }
        });
    }

    protected void addUnsweredQuestion(final Question question){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                answersAdapter.addQuestion(question);
            }
        });
    }

    @Override
    public void showError(int titleRes, int fixTitleres) {
        if (titleRes == R.string.networkError){
            Snackbar.make(radioGroup, titleRes, Snackbar.LENGTH_INDEFINITE)
                    .setAction(fixTitleres, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presenter.loadData(null);
                        }
                    })
                    .show();
        }else if (titleRes == R.string.no_questions){
            questionView.setText(R.string.no_questions);
        }else {
            Toast.makeText(this, titleRes, Toast.LENGTH_SHORT).show();
        }
    }

    private void buildDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setIndeterminate(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onNavItemClick(NavigationItem navigationItem, int position) {

    }

    @Override
    public void onCheckedChanged(final RadioGroup group, final int checkedId) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int index = radioGroup.indexOfChild(group.findViewById(checkedId));
                presenter.nextQuestion(index);
            }
        }, 500);
    }

    /**
     * Инициализирует вкладки
     */
    private void setupTabs(){
        int[] icons = {R.drawable.ic_check_box_black_24dp, R.drawable.ic_question_answer_black_24dp, R.drawable.ic_star_black_24dp};
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setCustomView(R.layout.item_openline_tab);
            ((ImageView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.icon)).setImageResource(icons[i]);
        }
    }

    public abstract OpenlinePresenter getPresenter();

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()){
            case 0:
                findViewById(R.id.answeredContainer).setVisibility(View.GONE);
                findViewById(R.id.rateContainer).setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                break;
            case 1:
                findViewById(R.id.answeredContainer).setVisibility(View.VISIBLE);
                findViewById(R.id.rateContainer).setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                break;
            case 2:
                findViewById(R.id.answeredContainer).setVisibility(View.GONE);
                findViewById(R.id.rateContainer).setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
