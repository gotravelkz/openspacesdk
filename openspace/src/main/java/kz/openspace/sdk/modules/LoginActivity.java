package kz.openspace.sdk.modules;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.R;

public abstract class LoginActivity extends AppCompatActivity implements BaseView, View.OnClickListener, DialogInterface.OnCancelListener {

    private EditText phoneEdit;
    private EditText countryCodeEdit;
    private EditText codeEdit;
    private LoginPresenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = getLoginPresenter();
        presenter.setView(this);
        presenter.loadData(null);
        setContentView(R.layout.login_activity);

        phoneEdit = (EditText) findViewById(R.id.phoneEdit);
        phoneEdit.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        codeEdit = (EditText) findViewById(R.id.codeEdit);
        countryCodeEdit = (EditText) findViewById(R.id.countryCode);
        ImageView logo = (ImageView) findViewById(R.id.logo);

        logo.setImageResource(getLogoResource());
        findViewById(R.id.sendSmsButton).setOnClickListener(this);
        findViewById(R.id.resendCodeButton).setOnClickListener(this);
        findViewById(R.id.sendCodeButton).setOnClickListener(this);
        findViewById(R.id.aboutAppText).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public void showData(Object data) {

    }

    @Override
    public void showLoading(final boolean loading) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
                if (loading)
                {
                    progressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.loading), getString(R.string.wait));
                    progressDialog.setOnCancelListener(LoginActivity.this);
                }
            }
        });
    }

    @Override
    public void showError(final int titleRes, int fixTitleRes) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, titleRes, Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void showCodeContainer(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.phoneContainer).setVisibility(View.GONE);
                findViewById(R.id.codeContainer).setVisibility(View.VISIBLE);
                findViewById(R.id.codeContainer).startAnimation(AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_in));
                codeEdit.requestFocus();
            }
        });
    }

    protected void showPhoneContainer(){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.phoneContainer).setVisibility(View.VISIBLE);
                findViewById(R.id.codeContainer).setVisibility(View.GONE);
                findViewById(R.id.phoneContainer).startAnimation(AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_in));
                phoneEdit.requestFocus();
                codeEdit.setText("");
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.sendSmsButton){
            String phoneNum = countryCodeEdit.getText().toString().replaceAll("[^\\d]", "");
            phoneNum += phoneEdit.getText().toString().replaceAll("[^\\d]", "");
            presenter.sendSmsCode(phoneNum);
        }else if (id == R.id.resendCodeButton){
            presenter.cancelAllRequests();
            showPhoneContainer();
        }else if (id == R.id.sendCodeButton){
            presenter.checkConfirmationCode(codeEdit.getText().toString());
        }else if (id == R.id.aboutAppText){
            startAdditionalActivity(getAboutAppIntentAction());
        }
    }

    private void startAdditionalActivity(String action){
        Intent intent = new Intent();
        intent.setAction(action);
        startActivity(intent);
    }

    /**
     * Возвращает логотип приложения с высотой не меньше 120dp
     * @return
     */
    public abstract int getLogoResource();

    public abstract LoginPresenter getLoginPresenter();

    /**
     * Срабатывает когда пользователь успешно проходит авторизацию
     */
    public abstract void onSignIn();

    /**
     * Возвращает Action для Intent, которое открывается при выборе текста "Как я могу войти без регистрации"
     * @return Intent Action
     */
    public abstract String getAuthInfoIntentAction();

    /**
     * Возвращает Action для Intent, которое открывается при выборе текста "О приложении"
     * @return Intent Action
     */
    public abstract String getAboutAppIntentAction();

    @Override
    public void onCancel(DialogInterface dialog) {
        presenter.cancelAllRequests();
    }
}
