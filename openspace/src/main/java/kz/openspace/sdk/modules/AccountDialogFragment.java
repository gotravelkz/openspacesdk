package kz.openspace.sdk.modules;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kz.openspace.sdk.R;

public class AccountDialogFragment extends DialogFragment implements View.OnClickListener {
    private SignInActionListener signInActionListener;
    private EditText usernameEdit;
    private EditText passwordEdit;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.signInActionListener = null;
    }

    public void setSignInActionListener(SignInActionListener signInActionListener) {
        this.signInActionListener = signInActionListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_auth, container, false);
        Button signUpButton = (Button) view.findViewById(R.id.signUp);
        Button signInButton = (Button) view.findViewById(R.id.signIn);
        usernameEdit = (EditText) view.findViewById(R.id.username);
        passwordEdit = (EditText) view.findViewById(R.id.password);

        signUpButton.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
//        super.onCancel(dialog);
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.signUp) {
            dismiss();
        }else if (i == R.id.signIn)
        {
            boolean inputValidated = validateUserInput();
            if (signInActionListener != null && inputValidated)
                signInActionListener.onUserSignIn(usernameEdit.getText().toString(), passwordEdit.getText().toString());
        }
    }

    private boolean validateUserInput(){
        boolean usernameValid = usernameEdit.getText().length() > 0;
        boolean passwordValid = passwordEdit.getText().length() > 0;
        if (!usernameValid || !passwordValid)
        {
            Toast.makeText(getContext(), R.string.invalid_format, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public interface SignInActionListener{
        void onUserSignIn(String username, String password);
    }

}
