package kz.openspace.sdk.modules;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.R;
import kz.openspace.sdk.model.User;

public abstract class AccountActivity extends AppCompatActivity implements BaseView<User>, CompoundButton.OnCheckedChangeListener, AccountDialogFragment.SignInActionListener {
    private static final String TAG_AUTH_DIALOG = "AuthDialog";
    private final String EMAIL_REG = "[\\w\\.-]{1,20}@[\\w\\.-]{1,20}\\.\\w{1,4}";
    private final String TEXT_REG = "[a-zA-Zа-яА-Я\\-]{1,}";
    private AccountPresenter presenter;
    private EditText iinEdit;
    private EditText emailEdit;
    private EditText nameEdit;
    private EditText lastnameEdit;
    private EditText midnameEdit;
    private EditText phoneEdit;
    private EditText passwordEdit;
    private EditText password2Edit;
//    private EditText passwordEdit;
    private Spinner ageSpinner;
    private Spinner genderSpinner;
    private Spinner socialStatusSpinner;
    private CheckBox residentCheckbox;
    private ProgressDialog dialog;
    private AccountDialogFragment accountDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        iinEdit = (EditText) findViewById(R.id.iin);
        emailEdit = (EditText) findViewById(R.id.email);
        nameEdit = (EditText) findViewById(R.id.name);
        midnameEdit = (EditText) findViewById(R.id.midname);
        lastnameEdit = (EditText) findViewById(R.id.lastname);
        phoneEdit = (EditText) findViewById(R.id.phone);
        passwordEdit = (EditText) findViewById(R.id.password);
        password2Edit = (EditText) findViewById(R.id.password2);

        genderSpinner = (Spinner) findViewById(R.id.gender);
        residentCheckbox = (CheckBox) findViewById(R.id.resident);
        socialStatusSpinner = (Spinner) findViewById(R.id.socialStatus);
        ageSpinner = (Spinner) findViewById(R.id.age);

        residentCheckbox = (CheckBox) findViewById(R.id.resident);
        residentCheckbox.setOnCheckedChangeListener(this);
        setPresenter(new AccountPresenter());
        presenter.setView(this);
        presenter.setToken(getToken());
        presenter.setAppName(getAppName());
        presenter.loadData(null);

        accountDialogFragment = new AccountDialogFragment();
        if (!presenter.userAuthorized()){
            accountDialogFragment.show(getSupportFragmentManager(), TAG_AUTH_DIALOG);
            accountDialogFragment.setSignInActionListener(this);
            findViewById(R.id.passwordContainer).setVisibility(View.VISIBLE);
            phoneEdit.setEnabled(false);
        }

    }

    protected AccountPresenter getPresenter(){
        return this.presenter;
    }

    public void setPresenter(AccountPresenter presenter){
        this.presenter = presenter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.save);
        menu.getItem(0).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != android.R.id.home) {
            boolean f = fieldsNotEmpty();
            if (!fieldsNotEmpty()){
                Toast.makeText(this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
                return true;
            }

            if(!validateInputData())
                return true;
            List<Integer> socialIds = intArrayToList(getResources().getIntArray(R.array.social_status_ids));
            int socialIdIndex = socialIds.get(socialStatusSpinner.getSelectedItemPosition());

            if (residentCheckbox.isChecked()){
                presenter.updateUser(emailEdit.getText().toString(),
                        Long.parseLong(iinEdit.getText().toString()),
                        nameEdit.getText().toString(),
                        lastnameEdit.getText().toString(),
                        midnameEdit.getText().toString(),
                        phoneEdit.getText().toString(),
                        socialIdIndex,
                        passwordEdit.getText().toString()
                );
            }else {
                presenter.updateUser(emailEdit.getText().toString(),
                        nameEdit.getText().toString(),
                        lastnameEdit.getText().toString(),
                        midnameEdit.getText().toString(),
                        phoneEdit.getText().toString(),
                        ageSpinner.getSelectedItemPosition(),
                        genderSpinner.getSelectedItemPosition(),
                        socialIdIndex,
                        passwordEdit.getText().toString()
                );
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showData(final User data) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                User user = data;
                if (user == null) {
                    return;
                }
                if (presenter.userAuthorized())
                {
                    findViewById(R.id.passwordContainer).setVisibility(View.GONE);
                    phoneEdit.setEnabled(true);
                }
                if (user.getIin() != null && user.getIin() > 0)
                    iinEdit.setText(String.valueOf(user.getIin()));
                if (user.isResident() == 1)
                    residentCheckbox.setChecked(true);
                nameEdit.setText(user.getFirstname());
                emailEdit.setText(user.getEmail());
                lastnameEdit.setText(user.getLastname());
                midnameEdit.setText(user.getMidname());
                ageSpinner.setSelection(user.getAge());
                genderSpinner.setSelection(user.getGender());

                int[] socialIds = getResources().getIntArray(R.array.social_status_ids);
                int index = intArrayToList(socialIds).indexOf(user.getSocialStatusId());
                int socialSpinnerItemsCount = socialStatusSpinner.getAdapter().getCount();
                if (index < socialSpinnerItemsCount && index >= 0)
                    socialStatusSpinner.setSelection(index);

                phoneEdit.setText(user.getPhoneNumber());
            }
        });
    }

    /**
     * Проверяет данные введенные пользователем на валидность
     * @return true если все поля заполнены правильно
     */
    private boolean validateInputData(){
        Pattern emailPattern = Pattern.compile(EMAIL_REG);
        String pass1 = passwordEdit.getText().toString();
        String pass2 = password2Edit.getText().toString();
        if (!presenter.userAuthorized() && !pass1.equals(pass2))
        {
            Log.d("LOG_TAG", getClass().getSimpleName() + ": pass " + password2Edit.getText() + " " + passwordEdit.getText());
            passwordEdit.requestFocus();
            Toast.makeText(this, R.string.passwordsNotSame, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (emailEdit.getText().length() > 0 && !emailPattern.matcher(emailEdit.getText()).matches()) {
            emailEdit.requestFocus();
            emailEdit.setError(getString(R.string.invalid_format));
            return false;
        }
        if (nameEdit.getText().length() > 0 && !isValidText(nameEdit.getText())){
            nameEdit.requestFocus();
            nameEdit.setError(getString(R.string.invalid_format));
            return false;
        }
        if (lastnameEdit.getText().length() > 0 && !isValidText(lastnameEdit.getText())){
            lastnameEdit.requestFocus();
            lastnameEdit.setError(getString(R.string.invalid_format));
            return false;
        }
        if (midnameEdit.getText().length() > 0 && !isValidText(midnameEdit.getText())){
            midnameEdit.requestFocus();
            midnameEdit.setError(getString(R.string.invalid_format));
            return false;
        }
        return true;
    }

    private boolean fieldsNotEmpty(){
        boolean resident = residentCheckbox.isChecked();
        if (resident && iinEdit.getText().length() <= 0)
        {
            return false;
        }
        if (!resident && (ageSpinner.getSelectedItemPosition() <= 0 || genderSpinner.getSelectedItemPosition() <= 0 || socialStatusSpinner.getSelectedItemPosition() <= 0))
        {
            return false;
        }
        if (emailEdit.getText().length() <= 0
                || nameEdit.getText().length() <= 0
                || lastnameEdit.getText().length() <= 0
                || midnameEdit.getText().length() <= 0
                || phoneEdit.getText().length() <= 0)
        {
            return false;
        }
        return true;
    }

    private boolean isValidText(CharSequence text){
        Pattern pattern = Pattern.compile(TEXT_REG);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    private List<Integer> intArrayToList(int[] array){
        List<Integer> newArray = new ArrayList<>();
        for (int i : array) {
            newArray.add(i);
        }
        return newArray;
    }

    @Override
    public void showLoading(final boolean loading) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (dialog != null)
                    dialog.dismiss();
                if (!loading)
                    return;
                dialog = ProgressDialog.show(AccountActivity.this, getString(R.string.loading), getString(R.string.wait));
            }
        });
    }

    @Override
    public void showError(final int titleRes, int fixTitleRes) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AccountActivity.this, titleRes, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked){
            iinEdit.setEnabled(true);
            genderSpinner.setEnabled(false);
            ageSpinner.setEnabled(false);
            iinEdit.requestFocus();
        }else {
            iinEdit.setEnabled(false);
            genderSpinner.setEnabled(true);
            ageSpinner.setEnabled(true);
            emailEdit.requestFocus();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public abstract String getToken();

    public abstract String getAppName();

    @Override
    public void onUserSignIn(String username, String password) {
        presenter.signIn(username, password);
    }

    public void dismissSignInDialog(){
        accountDialogFragment.dismiss();
    }

}
