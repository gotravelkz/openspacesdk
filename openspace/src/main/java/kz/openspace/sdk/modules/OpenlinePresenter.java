package kz.openspace.sdk.modules;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.data.NetworkRepository;
import kz.openspace.sdk.model.OpenlineInfo;
import kz.openspace.sdk.model.Question;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * <p>Загружает опросы из заданной URL-ссылки.</p>
 */
public abstract class OpenlinePresenter implements BasePresenter<OpenlineActivity>, Callback {
    private static final String OPENLINE_HOST = "api.openspace.kz";
    private static final String PATH_STATUS = "openline_status";
    private static final String USERNAME = "username";
    private static final String APP = "app";
    private static final String PATH_OPENLINE = "openline";
    protected String langParam = "lang";
    protected String answeredParam = "answered";
    protected String usernameParam = "username";
    protected String questionIdParam = "enquiry_id";
    protected String answerIdParam = "answer_id";
    protected String answeredCountParam = "count_answered_questions";
    protected String unansweredCountParam = "count_unanswered_questions";
    protected String userRateParam = "user_enquiry_rate";
    private OpenlineActivity view;
    private Context context;
    private NetworkRepository networkRepository;
    private List<Question> questions;
    private int currentQuestion;
    private OpenlineInfo openlineInfo;

    public OpenlinePresenter(Context context) {
        this.context = context;
        this.networkRepository = new NetworkRepository(10);
    }

    @Override
    public void setView(OpenlineActivity view) {
        this.view = view;
    }

    @Override
    public void loadData(Intent source) {
        if (OpenSpaceUtils.isNetworkAvailable(context))
        {
            view.showLoading(true);
            String url = getOpenlineUrl(0);
            networkRepository.call(url, this);
            getStatus();
            getAnsweredQuestions();
        }else {
            view.showError(R.string.networkError, R.string.repeat);
        }
    }

    @Override
    public void onViewDestroyed() {
        view = null;
        context = null;
        networkRepository = null;
        questions = null;
    }

    @Override
    public void onFailure(Call call, IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                view.showLoading(false);
                view.showError(R.string.networkError, R.string.repeat);
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        if (response.isSuccessful()) {
            this.questions = getQuestionsFromJson(response.body().string());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    view.showLoading(false);
                    currentQuestion = 0;
                    if (questions.size() <= 0)
                    {
                        view.showError(R.string.no_questions, 0);
                        return;
                    }
                    view.showData(questions.get(0));
                }
            });
            response.body().close();
        }
    }

    public void nextQuestion(int answerIndex){
        answer(questions.get(currentQuestion), answerIndex);
        getAnsweredQuestions();
        currentQuestion++;
        if (currentQuestion < questions.size())
        {
            view.showData(questions.get(currentQuestion));
        }else {
            view.showError(R.string.no_questions, 0);
        }
    }

    private void answer(final Question question, int answerIndex) {
        String url = getAnswerUrl();
        String data = usernameParam + "=" + getUsername()
                + "&" + questionIdParam + "=" + question.getId()
                + "&" + answerIdParam + "=" + (question.getAnswers().get(answerIndex)).getId();
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/html"), data);

        networkRepository.postRequest(url, requestBody, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200)
                {
                    openlineInfo.setAnsweredCount(openlineInfo.getAnswered() + 1);
                    openlineInfo.setUnansweredCount(openlineInfo.getUnanswered() - 1);
                    openlineInfo.setUserRate(openlineInfo.getUserRate() + question.getRate());
                    view.showOpenlineInfo(openlineInfo);
                } else {
                    view.showError(R.string.something_went_wrong, 0);
                }
            }
        });
    }

    private void getStatus(){
        networkRepository.getRequest(getStatusUrl(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("LOG_TAG", getClass().getSimpleName() + " status error " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200){
                    JsonObject statusObj = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    openlineInfo = new OpenlineInfo();
                    openlineInfo.setAnsweredCount(statusObj.get(answeredCountParam).getAsInt());
                    openlineInfo.setUnansweredCount(statusObj.get(unansweredCountParam).getAsInt());
                    openlineInfo.setUserRate(statusObj.get(userRateParam).getAsInt());
                    view.showOpenlineInfo(openlineInfo);
                }
                response.body().close();
            }
        });
    }

    protected void getAnsweredQuestions(){
        networkRepository.call(getOpenlineUrl(1), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200) {
                    List<Question> questions = getQuestionsFromJson(response.body().string());
                    view.setAnswersQuestions(questions);
                }
                response.body().close();
            }
        });
    }

    protected String getOpenlineUrl(int answered){
        HttpUrl.Builder builder =  getOpenlineUrlBuilder();
        builder.addQueryParameter(langParam, Locale.getDefault().getCountry().toLowerCase())
                .addQueryParameter(answeredParam, String.valueOf(answered))
                .addQueryParameter(usernameParam, getUsername())
                .addQueryParameter(APP, getApplicationName());
        return builder.build().toString();
    }

    @Override
    public OpenlineActivity getView() {
        return this.view;
    }

    /**
     * <p>Возвращает Builder ссылки на получение опросов.
     * Builder должен состоять из схемы, хоста и путей, ведущих к Api опросов.
     * Все остальные параметры OpenlinePresenter добавляет сам.
     * </p>
     *
     * @return
     */
    public HttpUrl.Builder getOpenlineUrlBuilder(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(OPENLINE_HOST)
                .addPathSegment(PATH_OPENLINE);
    }

    public String getAnswerUrl(){
        return getOpenlineUrlBuilder().toString();
    }

    /**
     * Возвращает url статусов опроса. URL должен содержать схему, хост, пути к статусам
     * и все остальные необходимые параметры.
     * @return
     */
    public String getStatusUrl(){
        return new HttpUrl.Builder()
                .scheme("http")
                .host(OPENLINE_HOST)
                .addEncodedPathSegment(PATH_STATUS)
                .addQueryParameter(APP, getApplicationName())
                .addQueryParameter(USERNAME, getUsername())
                .build().toString();
    }

    public abstract String getApplicationName();

    /**
     * Преобразует JSON-строку и возвращает список {@link Question}
     * @param json JSON-строка
     * @return список вопросов
     */
    public List<Question> getQuestionsFromJson(String json) {
        Gson gson = new Gson();
        List<Question> groups = gson.fromJson(json, new TypeToken<List<Question>>(){}.getType());
        return (List<Question>)(List)groups;
    }

    /**
     * Возвращает текущее имя пользователя
     * @return username
     */
    public abstract String getUsername();

}
