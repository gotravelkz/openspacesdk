package kz.openspace.sdk.modules;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Annotation;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import kz.openspace.sdk.R;
import kz.openspace.sdk.annotation.Navigated;
import kz.openspace.sdk.ui.BottomNavigationView;
import kz.openspace.sdk.ui.NavigationItem;

public abstract class NavigationMenu implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavClickListener {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private BottomNavigationView bottomNavigationView;
    private Context context;

    public NavigationMenu(Activity activity, int navigationViewId, int drawerLayoutId, int bottomNavigationId, int toolbarId) {
        this.context = activity;
//        navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
//        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
//        bottomNavigationView = (BottomNavigationView) activity.findViewById(R.id.bottomNav);
        navigationView = (NavigationView) activity.findViewById(navigationViewId);
        drawer = (DrawerLayout) activity.findViewById(drawerLayoutId);
        bottomNavigationView = (BottomNavigationView) activity.findViewById(bottomNavigationId);

        int menuRes = getMenuResource();
        int headerRes = getHeaderResource();
        int[] bottomNavIcons = getBottomNavigationIcons();
        String[] bottomNavTitles = getBottomNavigationTitles();

        if (menuRes != 0) {
            setupNavigationDrawer(activity, menuRes, toolbarId);
            navigationView.setNavigationItemSelectedListener(this);
        }

        if (headerRes != 0) {
            View headerView = LayoutInflater.from(activity).inflate(headerRes, null, false);
            navigationView.addHeaderView(headerView);
        }

        if (menuRes == 0 && headerRes == 0)
            disableNavDrawer();

        if (bottomNavIcons.length <= 1 || bottomNavTitles.length <= 1){
            disableBottomNav();
        }else {
            setupBottomNavigation(bottomNavIcons, bottomNavTitles);
            bottomNavigationView.setOnNavClickListener(this);
        }
    }

    public Context getContext(){
        return this.context;
    }

    public void setupNavigationDrawer(Activity activity, int menuResId, int toolbarId){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, (Toolbar) activity.findViewById(toolbarId),
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.inflateMenu(menuResId);
    }

    public void setupBottomNavigation(int[] icons, String[] titles){
        bottomNavigationView.addItems(titles, icons);
    }

    public void disableBottomNav(){
        CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) bottomNavigationView.getLayoutParams();
        p.setBehavior(null);
        bottomNavigationView.setLayoutParams(p);
        bottomNavigationView.setVisibility(View.GONE);
    }

    protected void disableNavDrawer(){
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }

    @Override
    public abstract boolean onNavigationItemSelected(@NonNull MenuItem item);

    @Override
    public abstract void onNavItemClick(NavigationItem navigationItem, int position);

    public abstract int getMenuResource();

    public abstract int getHeaderResource();

    public abstract int[] getBottomNavigationIcons();

    public abstract String[] getBottomNavigationTitles();
}
