package kz.openspace.sdk.modules;

import android.content.Intent;
import android.util.Log;

import com.orm.dsl.Ignore;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.NetworkRepository;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public abstract class RoutesPresenter implements BasePresenter<RoutesActivity> {
    private RoutesActivity view;
    private NetworkRepository networkRepository;
    protected String langParam = "lang";
    protected String offsetParam = "offset";
    protected String limitParam = "limit";
    protected Integer limitValue = 20;
    protected Integer offsetValue = 0;
    private DiskRepository diskRepository;

    public RoutesPresenter() {
        this.networkRepository = new NetworkRepository(10);
        this.diskRepository = getDiskRepository();
    }

    @Override
    public void setView(RoutesActivity view) {
        this.view = view;
    }

    @Override
    public void loadData(Intent source) {
        if (OpenSpaceUtils.isNetworkAvailable(view)){
            if (!networkRepository.isWorking())
                loadRoutesFromNetwork();
        }else {
            List list = diskRepository.getCachedData();
            view.showData(list);
        }
    }

    private void loadRoutesFromNetwork(){
        networkRepository.setWorking(true);
        view.showLoading(true);
        String url = getRoutesUrlBuilder()
                .addQueryParameter(limitParam, limitValue.toString())
                .addQueryParameter(offsetParam, offsetValue.toString())
                .addQueryParameter(langParam, Locale.getDefault().getCountry().toLowerCase())
                .build().toString();
        networkRepository.getRequest(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showLoading(false);
                view.showError(R.string.networkError, 0);
                networkRepository.setWorking(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200)
                {
                    List list = jsonToRoutes(response.body().string());
                    view.showData(list);
                    diskRepository.cacheData(list);
                }
                view.showLoading(false);
                networkRepository.setWorking(false);
            }
        });
    }

    @Override
    public void onViewDestroyed() {
        view = null;
        networkRepository = null;
    }

    @Override
    public RoutesActivity getView() {
        return this.view;
    }

    protected abstract HttpUrl.Builder getRoutesUrlBuilder();

    public abstract DiskRepository getDiskRepository();

    public abstract List jsonToRoutes(String json);

}
