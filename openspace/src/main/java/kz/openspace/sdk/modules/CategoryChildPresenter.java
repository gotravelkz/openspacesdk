package kz.openspace.sdk.modules;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.annotation.Categorized;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.NetworkRepository;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;

/**
 * <p>Загружает и связывает данные с классов {@link CategoryChildFragment}
 * Предполагается что ссылка на источник имеет параметры offset, limit и category_id.</p>
 * <p>Чтобы изменить названия этих параметров укажите в аннотации {@link Categorized}</p>
 */
public abstract class CategoryChildPresenter implements BasePresenter<BaseView>, Callback {
    protected String LIMIT = "limit";
    protected String OFFSET = "offset";
    protected String CATEGORY = "category_id";
    public static final int MAX_CACHE_COUNT = 40;
    private static final String LANGUAGE = "lang";
    private Context context;
    private NetworkRepository networkRepository;
    private DiskRepository diskRepository;
    private int offset = 0;
    private int limit = 10;
    private int category;
    private CategoryView view;
    private boolean clearItems = false;
    private boolean useLanguageParam = true;

    /**
     * Загружает данные для отдельной категории
     * @param context контекст
     * @param category категория. <b>Чтобы получить номер категории вызовите метод getCategory() в </b>{@link CategoryChildFragment}
     */
    public CategoryChildPresenter(Context context, int category) {
        this.context = context;
        this.category = category;
        this.networkRepository = new NetworkRepository(3);
        this.diskRepository = getDiskRepository();
        if (getClass().getAnnotation(Categorized.class) != null){
            LIMIT = getClass().getAnnotation(Categorized.class).limit();
            OFFSET = getClass().getAnnotation(Categorized.class).offset();
            CATEGORY = getClass().getAnnotation(Categorized.class).category();
        }
    }

    @Override
    public void setView(BaseView view) {
        this.view = (CategoryView) view;
    }

    protected String getUrl() {
        HttpUrl.Builder builder = getHostUrl();
        builder
                .addQueryParameter(CATEGORY, String.valueOf(category))
                .addQueryParameter(OFFSET, String.valueOf(offset))
                .addQueryParameter(LIMIT, String.valueOf(limit));
        String url = builder.build().toString();
        return url;
    }

    public void useLanguageParam(boolean useLang){
        this.useLanguageParam = useLang;
    }

    @Override
    public void loadData(Intent source) {
        if (OpenSpaceUtils.isNetworkAvailable(context)){
            if (networkRepository != null && !networkRepository.isWorking()) {
                networkRepository.call(getUrl(), this);
                networkRepository.setWorking(true);
                if (clearItems)
                    view.clearData();
                clearItems = false;
            }
        }else {
            view.clearData();
            view.showError(R.string.networkError, 0);
            view.showData(diskRepository.getCachedData());
            clearItems = true;
        }
    }

    @Override
    public void onFailure(Call call, IOException e) {
        if (networkRepository != null){
            networkRepository.setWorking(false);
        }
        if (OpenSpaceUtils.isNetworkAvailable(context))
            loadData(null);
        Log.w("LOG_TAG", getClass().getSimpleName() + ": loading news failed " );
    }

    @Override
    public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {
        if (!response.isSuccessful()) return;
        final List list = jsonToCategoryItems(response.body().string());
        networkRepository.setWorking(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.showLoading(false);
                if (list.size() > 0) {
                    view.showData(list);
                    offset += 10;
                }
                if (offset < MAX_CACHE_COUNT){
                    diskRepository.cacheData(list);
                }
            }
        });
    }

    private void runOnUiThread(Runnable runnable){
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    @Override
    public void onViewDestroyed() {
        if (networkRepository != null)
            networkRepository.cancel();
        view = null;
        diskRepository = null;
        networkRepository = null;
    }

    public int getCategory(){
        return category;
    }

    public void resetOffset(){
        this.offset = 0;
    }

    @Override
    public BaseView getView() {
        return this.view;
    }

    /**
     * Возвращает массив из элементов ссылки источника данных.
     * <p>1. Первым элементов в массиве всегда должна быть ссылка на источник данных вида sample.com
     * <b>без указания в ней https, http, слешей и других параметров</b></p>
     * <p>Все дополнительные сегменты путей должны быть указаны в последующих элементах массива</p>
     * <p>Например, для ссылки www.sample.com/api/v1/items массив будет выглядеть так:
     * ["sample.com", "api", "v1", "items"]
     * </p>
     * @return
     */
    public abstract HttpUrl.Builder getHostUrl();

    /**
     * Преобразует JSON-строку в массив данных используемого типа.
     * @param json JSON-строка
     * @return массив данных
     */
    public abstract List jsonToCategoryItems(String json);

    /**
     * Создает и возвращает экземпляр класса {@link DiskRepository}.
     * @return локальный репозиторий
     */
    public abstract DiskRepository getDiskRepository();
}
