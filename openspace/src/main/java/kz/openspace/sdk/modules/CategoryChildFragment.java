package kz.openspace.sdk.modules;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import kz.openspace.sdk.BuildConfig;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.adapter.LazyItemsAdapter;

/**
 * <p>Отображает список элементов отдельной категории внути {@link CategoriesActivity}</p>
 */
public abstract class CategoryChildFragment extends Fragment implements CategoryView, SwipeRefreshLayout.OnRefreshListener {
    private int category = 1;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private LazyItemsAdapter adapter;
    private CategoryChildPresenter presenter;
    private SwipeRefreshLayout swipe;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.adapter = getAdapter();
        if (BuildConfig.DEBUG && adapter == null){
            throw new NullPointerException("Adapter in Category fragment can not be null. " +
                    "Please override getAdapter() method in your fragment");
        }
        this.presenter = getPresenter();
        presenter.setView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        this.swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(onScrollListener);
        recyclerView.setAdapter(adapter);
        swipe.setOnRefreshListener(this);

        presenter.loadData(null);
        if (adapter.getItemCount() <= 0)
            adapter.showLoading(true);
        return view;
    }
        private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE
                    && layoutManager.findLastVisibleItemPosition() >= adapter.getItemCount() - 1
                    && OpenSpaceUtils.isNetworkAvailable(getContext())) {
                showLoading(true);
                presenter.loadData(null);
                layoutManager.scrollToPosition(adapter.getItemCount() - 1);
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
        presenter = null;

    }

    public void setCategory(int category){
        this.category = category;
    }

    /**
     * Возвращает номер категории текущего фрагмента
     * @return номер категории
     */
    public int getCategory(){
        return this.category;
    };

    @Override
    public void showData(Object data) {
        adapter.addItems((List) data);
    }

    @Override
    public void showLoading(boolean loading) {
        if (!loading) swipe.setRefreshing(loading);
        if (adapter.isProgressVisible() != loading)
        {
            adapter.showLoading(loading);
        }
    }

    @Override
    public void showError(int titleRes, int fixTitleres) {
        Toast.makeText(getContext(), titleRes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData(){
        adapter.clear();
    }

    /**
     * Создает и возвращает экземпляр класса {@link CategoryChildPresenter} или его наследника
     * @return
     */
    public abstract CategoryChildPresenter getPresenter();

    /**
     * Создает и возвращает адаптер {@link LazyItemsAdapter}.
     * @return
     */
    public abstract LazyItemsAdapter getAdapter();

    @Override
    public void onRefresh() {
        if (OpenSpaceUtils.isNetworkAvailable(getContext())) {
            clearData();
            presenter.resetOffset();
            presenter.loadData(null);
        }
        else {
            presenter.loadData(null);
            showLoading(false);
        }
    }
}
