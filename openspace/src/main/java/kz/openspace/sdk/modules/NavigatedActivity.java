package kz.openspace.sdk.modules;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;

import kz.openspace.sdk.R;
import kz.openspace.sdk.annotation.Navigated;
import kz.openspace.sdk.ui.BottomNavigationView;


/**
 * Родительский класс всех активити, в которых реализованы боковое и/или нижнее меню.
 * Для использования своего Layout-файла, необходимо переопределить методы
 * {@link #getToolbarId()}, {@link #getBottomNavigationViewId()}, {@link #getNavigationViewId()} , {@link #getDrawerLayoutId()}
 * и передать в них идентификаторы (id) своих view-элементов
 */
public abstract class NavigatedActivity extends AppCompatActivity{
    private NavigationMenu navigationMenu;
    private int bottomNavSelectedItem;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Navigated navigated = getClass().getAnnotation(Navigated.class);
        if (navigated != null){
            try {
                navigationMenu = navigated.value().getDeclaredConstructor(Activity.class, int.class, int.class, int.class, int.class).newInstance(
                        this,
                        getNavigationViewId(),
                        getDrawerLayoutId(),
                        getBottomNavigationViewId(),
                        getToolbarId());

                bottomNavSelectedItem = navigated.bottomSelectedItem();
                if (bottomNavSelectedItem >= 0)
                    navigationMenu.getBottomNavigationView().setItemSelected(bottomNavSelectedItem);
                else
                    navigationMenu.disableBottomNav();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }else {
            if (findViewById(getDrawerLayoutId()) != null || getDrawerLayoutId() == 0)
            {
                DrawerLayout drawerLayout = (DrawerLayout) findViewById(getDrawerLayoutId());
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }
            if (findViewById(getBottomNavigationViewId()) != null || getBottomNavigationViewId() == 0)
            {
                BottomNavigationView bottomNavigation = (BottomNavigationView) findViewById(getBottomNavigationViewId());
                CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) bottomNavigation.getLayoutParams();
                p.setBehavior(null);
                bottomNavigation.setLayoutParams(p);
                bottomNavigation.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Возвращает id элемента Toolbar
     * @return resource id
     */
    public int getToolbarId(){
        return R.id.toolbar;
    }

    /**
     * Возвращает id элемента NavigationView
     * @return resource id
     */
    public int getNavigationViewId() {
        return R.id.nav_view;
    }

    /**
     * Возвращает id элемента BottomNavigationView
     * @return resource id
     */
    public int getBottomNavigationViewId() {
        return R.id.bottomNav;
    }

    /**
     * Возвращает id элемента DrawerLayout
     * @return resource id
     */
    public int getDrawerLayoutId() {
        return R.id.drawer_layout;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (navigationMenu != null)
            navigationMenu.getBottomNavigationView().setItemSelected(bottomNavSelectedItem);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.navigationMenu = null;
    }

    public NavigationMenu getNavigationMenu(){
        return this.navigationMenu;
    }

}
