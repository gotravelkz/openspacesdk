package kz.openspace.sdk.modules;

import kz.openspace.sdk.BaseView;

public interface CategoryView extends BaseView {
    void clearData();
}
