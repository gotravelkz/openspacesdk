package kz.openspace.sdk.modules;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.data.Auth;
import kz.openspace.sdk.model.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public class AccountPresenter implements BasePresenter<AccountActivity> {
    private Auth auth = Auth.getInstance();
    private AccountActivity view;
    private User oldUser;
    private String token;
    private String appName;

    @Override
    public void setView(AccountActivity view) {
        this.view = view;
        oldUser = auth.getUser();
    }

    @Override
    public void loadData(Intent source) {
        User user = auth.getUser();
        view.showData(user);
    }

    public boolean userAuthorized(){
        return auth.getUser().isAuthorized() == 1;
    }

    protected void updateUser(String email, String name, String lastname, String midname, String phone, int ageId, int genderId, int socialId, String password){
        User user = new User();
        user.setUserId(oldUser.getUserId());
        user.setIin(oldUser.getIin());
        user.setIsRKResident(0);
        user.setPhoneNumber(phone);
        user.setEmail(email);
        user.setFirstname(name);
        user.setLastname(lastname);
        user.setMidname(midname);
        user.setAge(ageId);
        user.setGender(genderId);
        user.setSocialStatusId(socialId);
        user.setPassword(password);
        user.setAuthorized(auth.getUser().isAuthorized());
        sendUpdateRequest(user);
    }

    protected void updateUser(String email, Long iin, String name, String lastname, String midname, String phone, int socialId, String password){
        User user = new User();
        user.setUserId(oldUser.getUserId());
        user.setIsRKResident(1);
        user.setIin(iin);
        user.setEmail(email);
        user.setPhoneNumber(phone);
        user.setFirstname(name);
        user.setLastname(lastname);
        user.setMidname(midname);
        user.setSocialStatusId(socialId);
        user.setPassword(password);
        user.setAuthorized(auth.getUser().isAuthorized());
        sendUpdateRequest(user);
    }

    private void sendUpdateRequest(final User user){
        view.showLoading(true);
        auth.updateUserServerInfo(getUrl(), user, view.getToken(), view.getAppName(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showLoading(false);
                view.showError(R.string.networkError, 0);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                view.showLoading(false);
                if (response.code() == 200){
                    view.showError(R.string.changes_applied, 0);
                    String responseStr = response.body().string();
                    Gson gson = OpenSpaceUtils.createSugarAdaptedGson();
                    User tempUser = gson.fromJson(responseStr, User.class);
                    user.setIin(tempUser.getIin());
                    user.setGender(tempUser.getGender());
                    user.setAge(tempUser.getAge());
                    user.setAuthorized(tempUser.isAuthorized());
                    auth.saveUser(user);
                }else
                {
                    Log.d("LOG_TAG", getClass().getSimpleName() + ": resp " + response.body().string());
                }
            }
        });
    }

    // FIXME: 27.09.2016 Абстрагировать
    public String getUrl(){
        return "http://api.openspace.kz/spaceid";
    }

    public String getSignInUrl(){
        String url = "http://api.openspace.kz/spaceid_auth?" +
                "phone=%s" +
                "&username=%s" +
                "&password=%s" +
                "&token=%s" +
                "&device=android" +
                "&app=%s";
        return url;
    }

    @Override
    public void onViewDestroyed() {
        auth.cancelCheckingRequest();
        auth.cancelConfirmationRequest();
        auth.cancelSignInRequest();
        view = null;
        auth = null;
        signInCallback = null;
    }

    @Override
    public AccountActivity getView() {
        return view;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void signIn(String usename, String password) {
        String url = String.format(getSignInUrl(), auth.getUser().getPhoneNumber(), usename, password, token, appName);
        auth.signIn(url, signInCallback);
        view.showLoading(true);
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    private Callback signInCallback = new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {

        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if (response.code() == 200)
            {
                getUserInfo();
            }else
            {
                view.showError(R.string.something_went_wrong, 0);
                Log.d("LOG_TAG", getClass().getSimpleName() + ": error " + response.body().string());
            }
            view.showLoading(false);
        }
    };

    private void getUserInfo(){
        HttpUrl.Builder builder = new HttpUrl.Builder();
        builder.scheme("http")
                .host("api.openspace.kz")
                .addPathSegment("spaceid");
        auth.getUserFromServer(builder, auth.getUser().getPhoneNumber(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showError(R.string.networkError, 0);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                view.showLoading(false);
                if (response.code() == 200){
                    List<User> users = OpenSpaceUtils.createSugarAdaptedGson().fromJson(response.body().string(), new TypeToken<List<User>>(){}.getType());
                    User user = users.get(0);
                    user.setPhoneNumber(auth.getUser().getPhoneNumber());
                    auth.saveUser(user);
                    view.showData(user);
                    view.dismissSignInDialog();
                }else
                {
                    view.showError(R.string.something_went_wrong, 0);
                    Log.d("LOG_TAG", getClass().getSimpleName() + ": error " + response.body().string());
                }
            }
        });
    }

}
