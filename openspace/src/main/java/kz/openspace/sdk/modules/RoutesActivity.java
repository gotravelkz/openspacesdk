package kz.openspace.sdk.modules;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.adapter.LazyItemsAdapter;


/**
 * Отображает маршруты
 */
public abstract class RoutesActivity extends NavigatedActivity implements BaseView<List>, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private RoutesPresenter presenter;
    private LazyItemsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routes_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);

        this.adapter = getAdapter();
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout.setOnRefreshListener(this);

        this.presenter = getRoutesPresenter();
        presenter.setView(this);
        presenter.loadData(null);
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE
                    && layoutManager.findLastVisibleItemPosition() >= adapter.getItemCount() - 1
                    && OpenSpaceUtils.isNetworkAvailable(RoutesActivity.this)) {
                presenter.loadData(null);
                layoutManager.scrollToPosition(adapter.getItemCount() - 1);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public void showData(final List data) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter.addItems(data);
            }
        });
    }

    @Override
    public void showLoading(final boolean loading) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                adapter.showLoading(loading);
            }
        });
    }

    @Override
    public void showError(final int titleRes, int fixTitleRes) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RoutesActivity.this, titleRes, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public abstract RoutesPresenter getRoutesPresenter();

    public abstract LazyItemsAdapter getAdapter();

    @Override
    public void onRefresh() {
        adapter.clear();
        presenter.loadData(null);
    }

}
