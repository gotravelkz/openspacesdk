package kz.openspace.sdk.modules;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.R;
import kz.openspace.sdk.adapter.CategoryPagerAdapter;
import kz.openspace.sdk.annotation.Navigated;
import kz.openspace.sdk.model.Category;

/**
 * <p>Отображает категории загружаемые из интернета и
 * кешируемые в базу данных для оффлайн работы,
 * а также фрагменты отдельных категорий.</p>
 * <p>Чтобы включить отображение NavigationDrawer и BottomNavigationView
 * отметьте класс аннотацией {@link Navigated} и передайте ей соответствующие параметры.
 * Если будут переданы не все параметры для бокового или нижнего меню, то соответствующее
 * отсутствующим параметрам меню будет отключено
 * </p>
 * <p>Чтобы перехватывать события BottomNavigation и NavigationDrawer, переопределите методы
 * </p>
 */
public abstract class CategoriesActivity extends NavigatedActivity implements BaseView<List<Category>> {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CategoriesPresenter presenter;
    private CategoryPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categorized_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = getCategoryAdapter();
        presenter = getPresenter();
        presenter.setView(this);
        presenter.loadData(null);
    }

    /**
     * Возвращает адаптер категорий {@link CategoryPagerAdapter}
     * @return адаптер категорий
     */
    public abstract CategoryPagerAdapter getCategoryAdapter();

    /**
     * Возвращает презентер, наследник {@link CategoriesPresenter}
     * @return презентер
     */
    public abstract CategoriesPresenter getPresenter();

    @Override
    public void showData(List<Category> data) {
        adapter.setCategories(data);
        viewPager.setOffscreenPageLimit(data.size());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void showLoading(boolean loading) {

    }

    @Override
    public void showError(final int titleRes, int fixTitleres) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(CategoriesActivity.this, titleRes, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }
}
