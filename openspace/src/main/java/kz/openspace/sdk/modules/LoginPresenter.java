package kz.openspace.sdk.modules;

import android.content.Intent;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.data.Auth;
import kz.openspace.sdk.model.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

public abstract class LoginPresenter implements BasePresenter<LoginActivity> {
    private Auth auth;
    private LoginActivity view;
    private String phoneNumber;

    @Override
    public void setView(LoginActivity view) {
        this.view = view;
    }

    @Override
    public void loadData(Intent source) {
        auth = Auth.getInstance();
        if (auth.isAuthorized()){
            view.onSignIn();
        }
    }

    @Override
    public void onViewDestroyed() {
        view = null;
        auth = null;
    }

    /**
     * Отправляет код подтверждения на указанный номер
     * @param phoneNumber номер телефона
     */
    public void sendSmsCode(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        view.showLoading(true);
        auth.sendConfirmationCode(getAuthUrl(), phoneNumber, getToken(), getApplicationName(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showLoading(false);
                view.showError(R.string.networkError, 0);
                Log.d("LOG_TAG", getClass().getSimpleName() + " fail");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("LOG_TAG", getClass().getSimpleName() + " response " + response.body().string() + " code " + response.code());
                view.showLoading(false);
                if (response.code() == 200){
                    view.showCodeContainer();
                }else {
                    view.showError(R.string.wrong_phone, 0);
                }
            }
        });
    }

    /**
     * Проверяет введенный пользователем код подтверждения
     * @param code код подтверждения
     */
    public void checkConfirmationCode(String code){
        view.showLoading(true);
        auth.checkConfirmationCode(getAuthUrl(), phoneNumber, code, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showLoading(false);
                view.showError(R.string.networkError, 0);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200){
//                    User user = new User();
//                    user.setPhoneNumber(phoneNumber);
//                    auth.saveUser(user);
//                    view.showLoading(false);
//                    view.onSignIn();
                    getUserInfo();
                }else {
                    view.showError(R.string.wrong_sms_code, 0);
                    view.showLoading(false);
                }
            }
        });
    }

    private void getUserInfo(){
        HttpUrl.Builder builder = new HttpUrl.Builder();
        builder.scheme("http")
                .host("api.openspace.kz")
                .addPathSegment("spaceid");
        auth.getUserFromServer(builder, phoneNumber, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                view.showError(R.string.networkError, 0);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                view.showLoading(false);
                if (response.code() == 200){
                    List<User> users = OpenSpaceUtils.createSugarAdaptedGson().fromJson(response.body().string(), new TypeToken<List<User>>(){}.getType());
                    User tempUser = users.get(0);
//                    tempUser.setPhoneNumber(phoneNumber);
                    User user = new User();
                    user.setPhoneNumber(phoneNumber);
                    user.setUserId(tempUser.getUserId());
//                    user.setIin(tempUser.getIin());
                    auth.saveUser(user);
                    view.onSignIn();
                }
            }
        });
    }

    /**
     * <p>Возвращает URL-ссылку для запроса SMS кода.</p>
     * <p>Ссылка должна содержать схему, хост и пути к API</p>
     * @return
     */
    public abstract String getAuthUrl();

    public abstract String getApplicationName();

    public abstract String getToken();

    /**
     * Отменяет все запросы авторизации
     */
    public void cancelAllRequests() {
        auth.cancelConfirmationRequest();
    }

    @Override
    public LoginActivity getView() {
        return this.view;
    }
}
