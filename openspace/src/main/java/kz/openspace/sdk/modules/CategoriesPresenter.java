package kz.openspace.sdk.modules;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import java.io.IOException;
import java.util.List;

import kz.openspace.sdk.BasePresenter;
import kz.openspace.sdk.BaseView;
import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.data.DiskRepository;
import kz.openspace.sdk.data.NetworkRepository;
import kz.openspace.sdk.model.Category;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * <p>Загружает данные в "категоризированное активити"
 * <b>Для правильной работы класса модели категорий обязательно должны реализовывать интерфейс {@link Category}</b></p>
 * <p>{@link #getDiskRepository()} <i>Создание локального репозитория возлагается на клиента, т.к. запросы к базе данных для разных классов могут отличаться</i></p>
 */
public abstract class CategoriesPresenter implements BasePresenter<BaseView>, Callback {
    private BaseView view;
    private DiskRepository diskRepository;
    private NetworkRepository networkRepository;
    private Context context;

    public CategoriesPresenter(Context context) {
        this.diskRepository = getDiskRepository();
        this.networkRepository = new NetworkRepository(2);
        this.context = context;
    }

    @Override
    public void setView(BaseView view) {
        this.view = view;
    }

    @Override
    public void loadData(Intent source) {
        boolean networkAvailable = OpenSpaceUtils.isNetworkAvailable(context);
        if (networkAvailable) {
            networkRepository.call(getUrl(), this);
            return;
        }
        List<Category> categories = diskRepository.getCachedData();
        if (categories.size() > 0)
            view.showData(categories);
        else if (!networkAvailable)
            view.showError(R.string.networkError, 0);
    }

    @Override
    public void onViewDestroyed() {
        view = null;
        diskRepository = null;
        networkRepository = null;
        context = null;
    }

    @Override
    public void onFailure(Call call, IOException e) {
        // FIXME: Возможен выброс исключения
        // java.lang.IllegalStateException: Must be called from main thread of fragment host
        if (view != null)
            loadData(null);
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final List categories = jsonToCategories(response.body().string());
        if (view == null)
            return;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                view.showData(categories);
            }
        });
        diskRepository.clearCache();
        diskRepository.cacheData(categories);
    }

    @Override
    public BaseView getView() {
        return this.view;
    }

    /**
     * Преобразует JSON данные в список категорий.
     * @param json json строка получаемая из интернета
     * @return массив объектов реализующих интерфейс {@link Category}
     */
    public abstract List jsonToCategories(String json);

    /**
     * Возвращает экземпляр локального репозитория данных.
     * Метод является обязательным для переопределения, для
     * возможности кастомизации запросов к БД.
     * @return локальный репозиторий
     */
    public abstract DiskRepository getDiskRepository();

    /**
     * Возвращает URL-ссылку для интернет-репозитория
     * @return URL-ссылка источника категории
     */
    public abstract String getUrl();

}
