package kz.openspace.sdk.modules;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import kz.openspace.sdk.OpenSpaceUtils;
import kz.openspace.sdk.R;
import kz.openspace.sdk.adapter.AudioPlayerAdapter;
import kz.openspace.sdk.background.AudioPlayerService;
import kz.openspace.sdk.model.Track;


public class AudioPlayerFragment extends Fragment implements AudioPlayerAdapter.PlaybackButtonsClickListener, AudioPlayerService.PlayerStateChangeListener, DialogInterface.OnCancelListener {
    private static final int SD_PERMISSION_REQUEST = 124;
    private AudioPlayerService playerService;
    private AudioPlayerAdapter adapter;
    private ProgressDialog progressDialog;
    private String filesDir;
    private boolean isMarshmallow = OpenSpaceUtils.isHigherThanMarshmallow();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new AudioPlayerAdapter((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        adapter.setPlaybackButtonsClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout view = new LinearLayout(getContext());
        RecyclerView recyclerView = new RecyclerView(getContext());
        recyclerView.setId(R.id.recycler);
        view.addView(recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//                new AlertDialog.Builder(getContext())
//                .setTitle(R.string.loading)
//                .setMessage(R.string.wait)
//                .setCancelable(false)
//                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        playerService.cancelDownloading();
//                        dialog.dismiss();
//                    }
//                })
//                .create();

        return view;
    }

    private void showDialog(){
        this.progressDialog = ProgressDialog.show(
                getContext(),
                getString(R.string.loading),
                getString(R.string.wait),
                true);
        progressDialog.setOnCancelListener(this);
    }

    public void setTracks(List<Track> tracks){
        adapter.setTracks(tracks);
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = new Intent(getActivity(), AudioPlayerService.class);
        getContext().startService(intent);
        getContext().bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(getActivity(), AudioPlayerService.class);
        getContext().unbindService(connection);
        getContext().stopService(intent);
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            playerService = ((AudioPlayerService.PlayerBinder) service).getService();
            playerService.setPlayerStateChangeListener(AudioPlayerFragment.this);
            playerService.setFilesDir(filesDir);
            playerService.setTracks(adapter.getTracks());
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                initService();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private boolean permissionNeeded(){
        int permissionStatus = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (isMarshmallow && permissionStatus != PackageManager.PERMISSION_GRANTED){
            String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
            requestPermissions(permissions, SD_PERMISSION_REQUEST);
            return true;
        }
        return false;
    }

    private void initService(){
        playerService.checkDownloadedTracks();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            initService();
    }

    public void setFilesDir(String filesDir) {
        this.filesDir = filesDir;
    }

    @Override
    public void onPlaybackClick(Track track, int trackIndex) {
        if (permissionNeeded())
            return;
        if (playerService == null) {
            throw new NullPointerException("Service is null. Make sure you have declared "
                    + AudioPlayerService.class.getSimpleName() + " in your AndroidManifest.xml file?");
        }
        if (!track.isDownloaded())
        {
            adapter.notifyTrackDownloaded(trackIndex);
            showDialog();
            playerService.downloadFile(trackIndex);
        }
        else
        {
            if (!playerService.isPlaying() || playerService.getCurrentTrackIndex() != trackIndex){
                playerService.play(trackIndex);
            }else {
                playerService.pause();
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSeek(int trackIndex, int position) {
        playerService.seekTo(position);
    }

    @Override
    public void onReady() {
        progressDialog.dismiss();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onPositionUpdate(final int duration, final int position) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
//                int position = (playerService.getTrackDuration() * progress / 100);
                if (!adapter.isSeeking())
                    adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        playerService.cancelDownloading();
    }
}
