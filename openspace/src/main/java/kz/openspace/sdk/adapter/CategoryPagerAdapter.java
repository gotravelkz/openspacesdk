package kz.openspace.sdk.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.modules.CategoryChildFragment;
import kz.openspace.sdk.model.Category;

/**
 * * Класс адаптера категорий. Предполагается что
 * фрагменты будут созданы на стороне клиента.
 * Чтобы указать класс фрагментов клиента,
 * необходимо передать его класс в конструктор адаптера
 */
public class CategoryPagerAdapter extends FragmentStatePagerAdapter {
    private List<Category> categories;
    private Class<CategoryChildFragment> clazz;

    /**
     * Класс адаптера категорий. Предполагается что
     * фрагменты будут созданы на стороне клиента.
     * @param fm менеджер фрагментов
     */
    public CategoryPagerAdapter(FragmentManager fm, Class<? extends CategoryChildFragment> clazz) {
        super(fm);
        this.categories = new ArrayList<>();
        this.clazz = (Class<CategoryChildFragment>) clazz;
    }


    /**
     * Класс адаптера категорий. Предполагается что
     * фрагменты будут созданы на стороне клиента.
     * @param fm менеджер фрагментов
     * @param categories массив категорий
     */
    public CategoryPagerAdapter(FragmentManager fm, List<Category> categories, Class<CategoryChildFragment> clazz) {
        super(fm);
        this.categories = categories;
        this.clazz = clazz;
    }


    public void setCategories( List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        try {
            CategoryChildFragment fragment = clazz.newInstance();
            fragment.setCategory(categories.get(position).getCategoryId());
            return fragment;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).getName();
    }

    @Override
    public int getCount() {
        return categories.size();
    }
}
