package kz.openspace.sdk.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import kz.openspace.sdk.R;
import kz.openspace.sdk.model.Track;

public class AudioPlayerAdapter extends RecyclerView.Adapter<AudioPlayerAdapter.PlayerHolder> {
    private LayoutInflater inflater;
    private List<Track> tracks = new ArrayList<>();
    private PlaybackButtonsClickListener playbackButtonsClickListener;
    public static final String FORMAT_TIME = "%tM:%tS";
    private boolean isSeeking;

    public AudioPlayerAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
        notifyDataSetChanged();
    }

    public List<Track> getTracks() {
        return tracks;
    }


    public void setPlaybackButtonsClickListener(PlaybackButtonsClickListener playbackButtonsClickListener) {
        this.playbackButtonsClickListener = playbackButtonsClickListener;
    }

    public void notifyTrackDownloaded(int trackIndex){
        tracks.get(trackIndex).setDownloaded(true);
    }

    public boolean isSeeking() {
        return isSeeking;
    }

    @Override
    public PlayerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_audioplayer, parent, false);
        return new PlayerHolder(view);
    }

    @Override
    public void onBindViewHolder(PlayerHolder holder, int position) {
        Track track = tracks.get(position);
        if (!track.isDownloaded()){
            holder.playback.setImageResource(R.drawable.ic_file_download_black_24dp);
            holder.seekBar.setEnabled(false);
            return;
        }
        if (track.isPlaying()){
            holder.playback.setImageResource(R.drawable.ic_pause_black_24dp);
        }else
        {
            holder.playback.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
        long millis = track.getPositionInMillis();
        holder.time.setText(String.format(FORMAT_TIME, millis, millis));
        holder.seekBar.setEnabled(true);
        holder.seekBar.setProgress(track.getPosition());
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

    class PlayerHolder extends RecyclerView.ViewHolder implements SeekBar.OnSeekBarChangeListener {
        private ImageView playback;
        private SeekBar seekBar;
        private TextView time;

        public PlayerHolder(View itemView) {
            super(itemView);
            playback = (ImageView) itemView.findViewById(R.id.playback);
            seekBar = (SeekBar) itemView.findViewById(R.id.seekBar);
            time = (TextView) itemView.findViewById(R.id.time);
            playback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playbackButtonsClickListener != null){
                        playbackButtonsClickListener.onPlaybackClick(tracks.get(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
            seekBar.setOnSeekBarChangeListener(this);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (playbackButtonsClickListener != null && fromUser)
            {
                playbackButtonsClickListener.onSeek(getAdapterPosition(), progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            isSeeking = true;
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            isSeeking = false;
        }
    }

    public interface PlaybackButtonsClickListener{
        void onPlaybackClick(Track track, int trackIndex);
        void onSeek(int trackIndex, int position);
    }

}
