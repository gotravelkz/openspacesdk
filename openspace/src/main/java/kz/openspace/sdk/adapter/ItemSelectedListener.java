package kz.openspace.sdk.adapter;

/**
 * Created by mancj on 20.09.2016.
 */

public interface ItemSelectedListener {
    void onItemSelected(Object item, int position);
}
