package kz.openspace.sdk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.R;

public abstract class LazyItemsAdapter<E extends RecyclerView.ViewHolder> extends RecyclerView.Adapter{
    private static final int TYPE_LOADING = 0;
    private static final int TYPE_ITEM = 1;
    private List data = new ArrayList();
    private LayoutInflater inflater;

    public LazyItemsAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    /**
     * Удаляет все данные из списка
     */
    public void clear(){
        data.clear();
        notifyDataSetChanged();
    }

    /**
     * Отображает загрузку элементов
     * @param loading
     */
    public void showLoading(boolean loading){
        if (loading){
            if (!data.contains(null)){
                data.add(null);
            }
        }else {
            if (data.contains(null)){
                data.remove(null);
            }
        }
        notifyDataSetChanged();
    }

    /**
     * Добавляет данные в список
     * @param items массив данных
     */
    public void addItems(List items){
        data.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * Возвращает LayoutInflater для создания View
     * @return
     */
    public LayoutInflater getLayoutInflater(){
        return this.inflater;
    }

    public boolean isProgressVisible(){
        return data.contains(null);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_LOADING){
            View view = inflater.inflate(R.layout.item_list_progress, parent, false);
            return new ProgressHolder(view);
        }
        return onCreateItemHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_ITEM){
            onBindItemHolder((E) holder, position);
        }
    }

    /**
     * Возвращает данные из списка
     * @return
     */
    public List getDataItems(){
        return this.data;
    }

    public abstract E onCreateItemHolder(ViewGroup parent, int viewType);

    public abstract void onBindItemHolder(E holder, int position);

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) == null){
            return TYPE_LOADING;
        }
        return TYPE_ITEM;
    }

    protected class ProgressHolder extends RecyclerView.ViewHolder{
        public ProgressHolder(View itemView) {
            super(itemView);
        }
    }
}
