package kz.openspace.sdk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kz.openspace.sdk.R;
import kz.openspace.sdk.model.Answer;
import kz.openspace.sdk.model.Question;

public class OpenLineAnsweredAdapter extends RecyclerView.Adapter<OpenLineAnsweredAdapter.QuestionHolder> {
    private List<Question> questions = new ArrayList<>();
    private LayoutInflater inflater;

    public OpenLineAnsweredAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public void setQuestions(List<Question> questions){
        this.questions = questions;
        notifyDataSetChanged();
    }

    public void addQuestion(Question question){
        this.questions.add(question);
        notifyDataSetChanged();
    }

    @Override
    public QuestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_openline_answered, parent, false);
        return new QuestionHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionHolder holder, int position) {
        Question question = questions.get(position);
        holder.question.setText(question.getName());
        for (Object answer : question.getAnswers()) {
            if (((Answer) answer).isSelected()){
                holder.answer.setText(((Answer) answer).getName());
            }
        }
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    static class QuestionHolder extends RecyclerView.ViewHolder{
        protected TextView question;
        protected TextView answer;

        public QuestionHolder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question);
            answer = (TextView) itemView.findViewById(R.id.answer);
        }
    }

}
