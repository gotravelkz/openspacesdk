package kz.openspace.sdk.data;

public class Where {
    private String clause;
    private int arg;

    public Where(String clause, int arg) {
        this.clause = clause;
        this.arg = arg;
    }

    public Where() {
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause;
    }

    public int getArg() {
        return arg;
    }

    public void setArg(int arg) {
        this.arg = arg;
    }

}
