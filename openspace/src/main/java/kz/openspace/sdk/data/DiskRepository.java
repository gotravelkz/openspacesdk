package kz.openspace.sdk.data;

import android.util.Log;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import junit.framework.Test;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * <p>Кеширует данные в базу данных и возвращает сохраненные объекты.</p>
 * <p><i>Все сущности используемые для сохрнанения в кеш должны наследоваться от класса SugarRecord.
 * Для правильной работы класса одно из полей модели обязательно должно быть помечено аннотацией {@link com.orm.dsl.Unique}</i></p>
 * <p>Чтобы указывать дополнительные параметры выборки, используйте метод {@link #addWhereClause(Where)}</p>
 * <p><b>Примечание: модели могут иметь отношения one-to-many (SQL relationships).</p>
 * @param <E> Тип кешируемых объектов.
 */
public class DiskRepository<E extends SugarRecord>{
    private int maxCacheCount;
    private Class clazz;
    private Select<E> select;
    private DiskCacheListener diskCacheListener;

    /**
     * @param clazz тип класса кешируемых объектов
     * @param maxCacheCount максимальное количетсво кешируемых объектов.
     <p>Если {@link #maxCacheCount} <= 0, все переданные записи будут сохраняться в бд.
     * В противном случае сохраняться будет столько записей, сколько указано в {@link #maxCacheCount}</p>
     */
    public DiskRepository(Class clazz, int maxCacheCount) {
        this.maxCacheCount = maxCacheCount;
        this.clazz = clazz;
        this.select = Select.from(clazz);
    }

    public void setDiskCacheListener(DiskCacheListener diskCacheListener) {
        this.diskCacheListener = diskCacheListener;
    }

    /**
     * Добавляет параметры выборки в запрос (Sql where)
     * @param where параметры
     */
    public void addWhereClause(Where where){
        select.where(Condition.prop(where.getClause().toUpperCase()).eq(where.getArg()));
    }

    /**
     * Сохраняет данные в базу данных
     * @param data массив данных
     */
    public synchronized void cacheData(final List<E> data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long count = select.count();
                List<E> old = select.orderBy("ID DESC").limit(String.valueOf(maxCacheCount)).list();
                for (E e : data) {
                    e.update();
                    if (diskCacheListener != null)
                        diskCacheListener.onItemCached(e);
                    count++;
                    if (maxCacheCount > 0 && count > maxCacheCount){
                        old.get(old.size() - 1).delete();
                        old.remove(old.size() - 1);
                    }
                }
            }
        }).start();
    }

    /**
     * Сохраняет единственный элемент в базу данных
     * @param item
     */
    public void cacheSingleDataItem(E item){
        item.update();
        if (diskCacheListener != null)
            diskCacheListener.onItemCached(item);
    }

    /**
     * Очищает все записи из базы данных
     */
    public synchronized void clearCache(final Class<? extends SugarRecord> ... relatedObjects){
        new Thread(new Runnable() {
            @Override
            public void run() {
                E.deleteAll(clazz);
            }
        }).start();
    }

    /**
     * Удаляет указанный элемент из кеша
     * @param item удаляемый элемент
     */
    public void deleteCacheItem(E item){
        if (diskCacheListener != null)
            diskCacheListener.onItemDeleted(item);
        E.delete(item);
    }

    public E getSingleCacheItem(){
        return select.first();
    }

    /**
     * Возвращает данные, сохраненные в базе данных
     * @return массив данных
     */
    public List<E> getCachedData() {
        return select.list();
    }
}
