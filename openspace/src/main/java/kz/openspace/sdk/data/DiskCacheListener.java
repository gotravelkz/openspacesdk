package kz.openspace.sdk.data;

import com.orm.SugarRecord;

/**
 * Created by mancj on 20.09.2016.
 */

public interface DiskCacheListener<E extends SugarRecord> {
    void onItemCached(E item);
    void onItemDeleted(E item);
}
