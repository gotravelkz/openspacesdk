package kz.openspace.sdk.data;

import java.util.Locale;
import kz.openspace.sdk.model.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Отвечает за авторизацию пользователя через СМС.
 * На данный момент отправка и проверка кода смс через openspace api
 * проходит по одной ссылке, но разными методами (POST и PUT).
 */
public class Auth {
    private static final Object SMS_SEND = "sms_send";
    private static final String PHONE_NUMBER = "phone";
    private static final String CODE = "code";
    private static final int SMS_SEND_VALUE = 1;
    private static final String LANGUAGE = "lang";
    private static final String TOKEN = "token";
    private static final String APP_NAME = "app";
    private static Auth ourInstance = new Auth();
    private User user;
    private DiskRepository<User> repository;
    private OkHttpClient client;
    private Call sendCall;
    private Call checkCall;
    private final String USER_INFO_STRING = "id=%d&phone=%s&is_resident=%s&username=%s" +
            "&email=%s&last_name=%s&first_name=%s&middle_name=%s&social_status_id=%d";
    private final String NOT_RESIDENT_DATA = "&gender_id=%d&age_id=%d";
    public static final String PASSWORD = "&password=%s";
    public static final String APP_INFO = "&token=%s&device=android&app=%s";
    private Call signInCall;

    public static Auth getInstance() {
        return ourInstance;
    }

    private Auth() {
        this.client = new OkHttpClient();
        this.repository = new DiskRepository<>(User.class, 0);
    }

    public boolean isAuthorized(){
        this.user = repository.getSingleCacheItem();
        return user != null;
    }

    public User getUser(){
        this.user = repository.getSingleCacheItem();
        return this.user;
    }

    /**
     * Удаляет пользователя из базы данных
     */
    public void logOut(){
        User.deleteAll(User.class);
    }

    /**
     * Отправляет код подтверждения на указанный номер телефона
     * @param url ссылка на Api
     * @param phoneNumber номер телефона
     * @param callback коллбэк-функция, вызывается при получении ответа от сервера
     */
    public void sendConfirmationCode(String url, String phoneNumber, String token, String appName, Callback callback){
        String data = PHONE_NUMBER + "=" + phoneNumber + "&" + SMS_SEND + "=" + SMS_SEND_VALUE
                + "&device=android&" + TOKEN + "=" + token + "&" + APP_NAME + "=" + appName;
        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("text/html"), data))
                .build();
        sendCall = client.newCall(request);
        sendCall.enqueue(callback);
    }

    /**
     * Проверяет код подтверждения указанный пользователем
     * @param url ссылка на Api
     * @param phoneNumber номер телефона
     * @param confirmationCode код подтверждения
     * @param callback коллбэк-функция, вызывается при получении ответа от сервера
     */
    public void checkConfirmationCode(String url, String phoneNumber, String confirmationCode, Callback callback){
        String data = PHONE_NUMBER + "=" + phoneNumber + "&" + CODE + "=" + confirmationCode;
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(MediaType.parse("text/html"), data))
                .build();
        checkCall = client.newCall(request);
        checkCall.enqueue(callback);
    }

    public void getUserFromServer(HttpUrl.Builder host, String phoneNUmber, Callback callback){
        host.addQueryParameter(PHONE_NUMBER, phoneNUmber)
                .addQueryParameter(LANGUAGE, Locale.getDefault().getCountry().toLowerCase())
                .build();
        Request request = new Request.Builder()
                .url(host.toString())
                .build();
        Call userCall = client.newCall(request);
        userCall.enqueue(callback);
    }

    public void updateUserServerInfo(String url, User user, String token, String appName, Callback callback){
        String data = String.format(USER_INFO_STRING,
                user.getUserId(),
                user.getPhoneNumber(),
                user.isResident(),
                user.getIin(),
                user.getEmail(),
                user.getLastname(),
                user.getFirstname(),
                user.getMidname(),
                user.getSocialStatusId());
        if (user.isResident() == 0)
        {
            data += String.format(NOT_RESIDENT_DATA,
                    user.getGender(),
                    user.getAge());

        }
        if (user.isAuthorized() == 0 && user.getPassword().length() > 0)
        {
            data += String.format(PASSWORD, user.getPassword());
        }
        data += String.format(APP_INFO, token, appName);
        Request request = new Request.Builder()
                .url(url)
                .put(RequestBody.create(MediaType.parse(""), data))
                .build();
        Call updateUserCall = client.newCall(request);
        updateUserCall.enqueue(callback);
    }

    public void signIn(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .build();
        signInCall = client.newCall(request);
        signInCall.enqueue(callback);
    }

    /**
     * Сохраняет пользователя
     * @param phoneNumber
     */
    public void saveUser(String phoneNumber){
        this.user = new User();
        user.setPhoneNumber(phoneNumber);
        repository.cacheSingleDataItem(user);
    }

    /**
     * Сохраняет пользователя
     */
    public void saveUser(User user){
        this.user = user;
        repository.cacheSingleDataItem(user);
    }

    /**
     * Отменяет запрос проверки кода подтверждения
     */
    public void cancelCheckingRequest(){
        if (checkCall != null && !checkCall.isCanceled())
            checkCall.cancel();
    }

    /**
     * Отменяет запрос отправки СМС-сообщения
     */
    public void cancelConfirmationRequest() {
        if (sendCall != null && !sendCall.isCanceled())
            sendCall.cancel();
    }

    public void cancelSignInRequest(){
        if (signInCall != null && !signInCall.isCanceled())
            signInCall.cancel();
    }

}
