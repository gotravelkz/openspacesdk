package kz.openspace.sdk.data;

import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Получает данные из интернета
 */
public class NetworkRepository {
    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    private boolean working;
    private OkHttpClient client;
    private Call call;

    public NetworkRepository(int timeoutInSeconds) {
        client = new OkHttpClient.Builder()
                .connectTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .build();
    }

    public void call(String url, Callback callback){
        Request request = new Request.Builder()
                .url(url)
                .build();
        call = client.newCall(request);
        call.enqueue(callback);
    }

    public void postRequest(String url, RequestBody requestBody, Callback callback){
        Request request = new Request.Builder()
            .url(url)
            .post(requestBody)
            .build();
        client
            .newCall(request)
            .enqueue(callback);
    }

    public void getRequest(String url, Callback callback){
        Request request = new Request.Builder()
                .url(url)
                .build();
        call = client.newCall(request);
        call.enqueue(callback);
    }

    public void cancel(){
        if (call != null)
            call.cancel();
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }
}
