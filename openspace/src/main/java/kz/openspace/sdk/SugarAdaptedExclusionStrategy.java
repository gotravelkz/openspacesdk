package kz.openspace.sdk;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.orm.SugarRecord;

/**
 * Created by mancj on 20.09.2016.
 */
public class SugarAdaptedExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        if (f.getDeclaringClass().equals(SugarRecord.class) && f.getName().equals("id"))
            return true;
        return false;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        if (clazz.equals(SugarRecord.class))
            return true;
        return false;
    }

}
