package kz.openspace.sdk;

/**
 * Базовый View-класс, родительский класс всех View
 */
public interface BaseView<T> {
    /**
     * Отображает данные загруженные через презентер
     * @param data массив или объект данных
     */
    void showData(T data);

    /**
     * Отображает загрузку данных
     * @param loading статус загрузки
     */
    void showLoading(boolean loading);


    /**
     * Выдает ошибку в случае неудачи загрузки данных и т.п.
     * @param titleRes заголовок
     * @param fixTitleRes текст, объясняющий как исправить ошибку. Обычно это "повторить";
     */
    void showError(int titleRes, int fixTitleRes);

}
