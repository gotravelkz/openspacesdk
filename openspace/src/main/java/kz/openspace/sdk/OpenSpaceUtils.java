package kz.openspace.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OpenSpaceUtils {

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Gson createSugarAdaptedGson(){
        Gson gson = new GsonBuilder()
                .addDeserializationExclusionStrategy(new SugarAdaptedExclusionStrategy())
                .addSerializationExclusionStrategy(new SugarAdaptedExclusionStrategy())
                .create();
        return gson;
    }

    public static boolean isHigherThanMarshmallow(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

}