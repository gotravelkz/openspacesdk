package kz.openspace.sdk;

import okhttp3.Response;

public interface CallbackListener {
    void onCallback(int requestCode, int status, Response response);
}
